//
//  RecentsViewController.swift
//  ClickToChat
//
//  Created by Gabani King on 28/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import MessageUI
import IQKeyboardManager

class RecentsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMessageComposeViewControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    
    var arrKeypadNumberSearching: [CCKeypadNumber] = [CCKeypadNumber]()
    
    var isSearching: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        tblView.delegate = self
        tblView.dataSource = self
        txtSearch.delegate = self
        
        self.txtSearch.addTarget(self, action: #selector(searchWorkersAsPerText(_ :)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }
    
    // MARK: - TextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       
        textField.resignFirstResponder()
        return true
    }
    
    
    @objc func searchWorkersAsPerText(_ textfield:UITextField) {
        
        if textfield.text == "" {
            self.isSearching = false
            self.tblView.reloadData()
        }
        else
        {
            self.isSearching = true
            
            self.arrKeypadNumberSearching.removeAll(keepingCapacity: false)
            
            for i in 0..<appDelegate.arrKeypadNumber.count {
                
                let listItem: CCKeypadNumber = appDelegate.arrKeypadNumber[i]
                let phone = listItem.userMobile ?? ""
                let name = listItem.UserName ?? ""
                let message = listItem.UserMessage ?? ""
                let UserCode = listItem.UserCode ?? ""
                
                if listItem.UserName!.lowercased().range(of: self.txtSearch.text!.lowercased()) != nil ||  listItem.UserMessage!.lowercased().range(of: self.txtSearch.text!.lowercased()) != nil || "\(name) (\(message))".lowercased().range(of: self.txtSearch.text!.lowercased()) != nil || "\(UserCode) \(phone)".lowercased().range(of: self.txtSearch.text!.lowercased()) != nil {
                    self.arrKeypadNumberSearching.append(listItem)
                }
                
            }
            
            self.tblView.reloadData()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tblView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSearching == true {
            return arrKeypadNumberSearching.count
        }else {
            return appDelegate.arrKeypadNumber.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "RecentsTableCell") as! RecentsTableCell
        
        var dicData: CCKeypadNumber!
        if self.isSearching == true {
            dicData = self.arrKeypadNumberSearching[indexPath.row]
        } else {
            dicData = appDelegate.arrKeypadNumber[indexPath.row]
        }
        
        let phone = dicData.userMobile ?? ""
        let name = dicData.UserName ?? ""
        let message = dicData.UserMessage ?? ""
        let type = dicData.UserType ?? ""
        let UserCode = dicData.UserCode ?? ""
        
        cell.lblType.text = type
        cell.lblPhoneNumber.text = "\(UserCode) \(phone)"
        
        if message != "" && name != "" {
            cell.lblNameMessage.text = "\(name) (\(message))"
        }
        else if name != "" && message == ""
        {
            cell.lblNameMessage.text = "\(name) (No Message)"
        }
        else if message == "" && name == ""
        {
            cell.lblNameMessage.text = "(No Message)"
        }
        else
        {
            cell.lblNameMessage.text = "\(message)"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var dicData: CCKeypadNumber!
        if self.isSearching == true {
            dicData = self.arrKeypadNumberSearching[indexPath.row]
        } else {
            dicData = appDelegate.arrKeypadNumber[indexPath.row]
        }
        
        appDelegate.dicKeypadNumber = dicData
        appDelegate.isUpdateKeypad = true
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home: TabViewController = mainStoryboard.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
        home.selectedIndex = 3
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        appDelegate.window?.rootViewController = homeNavigation
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return 60
        }
        else
        {
            return 90
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        var dicData: CCKeypadNumber!
        if self.isSearching == true {
            dicData = self.arrKeypadNumberSearching[indexPath.row]
        } else {
            dicData = appDelegate.arrKeypadNumber[indexPath.row]
        }
        
        let editAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: { (action, indexPath) in
            print("Delete tapped")
            
            let alert = UIAlertController(title: "Alert", message: "Are you sure, you want delete?", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            })
            alert.addAction(ok)
            let cancel = UIAlertAction(title: "Delete", style: .destructive, handler: { action in
                
                if self.isSearching == true
                {

                } else
                {
                    appDelegate.arrKeypadNumber.remove(at: indexPath.row)
                    appDelegate.saveKeypadNumberList(array: appDelegate.arrKeypadNumber)
                    self.tblView.reloadData()
                }
                
               
            })
            
            alert.addAction(cancel)
            self.present(alert, animated: true)
            
        })
        editAction.backgroundColor = UIColor.red
        
        let deleteAction = UITableViewRowAction(style: .default, title: "Resend", handler: { (action, indexPath) in
            print("Edit tapped")
            
            if dicData.UserType == "Text Message"
            {
                let messageVC = MFMessageComposeViewController()
                messageVC.body = dicData.UserMessage ?? "";
                messageVC.recipients = ["\(dicData.UserCode ?? "")\(dicData.userMobile ?? "")"]
                messageVC.messageComposeDelegate = self
                self.present(messageVC, animated: true, completion: nil)
            }
            else
            {
                
                let urlWhats = "whatsapp://send?phone=\("\(dicData.UserCode ?? "")\(dicData.userMobile ?? "")")&text=\(dicData.UserMessage ?? "")"
                if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                    if let whatsappURL = NSURL(string: urlString) {
                        if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                            UIApplication.shared.open(whatsappURL as URL)
                            
                        }
                        else {
                            print("please install watsapp")
                        }
                    }
                }
            }
            
        })
        
        deleteAction.backgroundColor = App_Color_Blue
        
        return [editAction, deleteAction]
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
        case .failed:
            print("Message failed")
        case .sent:
            print("Message was sent")
        default:
            return
        }
        dismiss(animated: true, completion: nil)
    }
    
}
