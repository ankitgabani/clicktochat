//
//  MoreViewController.swift
//  ClickToChat
//
//  Created by Gabani King on 28/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import MessageUI

class MoreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    
    var arrName = ["Share","Rate Us","Contact Us","More Apps"]
    var arrName1 = ["Remove Ads","Restore Purchase"]
    
    var arrImg = ["ic_Share","ic_Rate","ic_Message","ic_MoreApp"]
    var arrImg1 = ["ic_RemoveAds","ic_Restore"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0
        {
            return arrName.count
        }
        else
        {
            return arrName1.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "MoreTableViewCell") as! MoreTableViewCell
        
        if indexPath.section == 0
        {
            cell.lblANme.text = arrName[indexPath.row]
            
            cell.imgMore.image = UIImage(named: arrImg[indexPath.row])
        }
        else
        {
            cell.lblANme.text = arrName1[indexPath.row]
            
            cell.imgMore.image = UIImage(named: arrImg1[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0
        {
            if indexPath.row == 0
            {
                AppUse.shareApp(inController: self)
            }
            else if indexPath.row == 1
            {
                AppUse.rateApp()
            }
            else if indexPath.row == 2
            {
                if MFMailComposeViewController.canSendMail() {
                    let mail = MFMailComposeViewController()
                    mail.mailComposeDelegate = self
                    mail.setToRecipients(["manufacturingbiss@gmail.com"])
                    mail.setSubject("Feedback for ClickToChat iOS")
                    
                    present(mail, animated: true)
                } else {
                    // show failure alert
                }
            }
            else if indexPath.row == 3
            {
                AppUse.rateAppDeveloper()
            }
        }
        else
        {
            if indexPath.row == 0
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "InAppPurchaseVC") as! InAppPurchaseVC
                self.present(vc, animated: true, completion: nil)
            }
            else if indexPath.row == 1
            {
                
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            return 60
        }
        else
        {
            return 85
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0
        {
            return UIView()
        }
        else
        {
            
            let headerView = Bundle.main.loadNibNamed("MoreHeaderView", owner: self, options: [:])?.first as! MoreHeaderView
            
            return headerView
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            if section == 0
            {
                return 1
            }
            else
            {
                return 132
            }
        }
        else
        {
            if section == 0
            {
                return 1
            }
            else
            {
                return 145
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        
        if section == 0
        {
            return UIView()
        }
        else
        {
            let footerView = Bundle.main.loadNibNamed("MoreFooterView", owner: self, options: [:])?.first as! MoreFooterView
            
            footerView.btnPP.addTarget(self, action: #selector(clickedPP(sender:)), for: .touchUpInside)
            footerView.btnTC.addTarget(self, action: #selector(clickedTC(sender:)), for: .touchUpInside)
            
            return footerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            if section == 0
            {
                return 1
            }
            else
            {
                return 65
            }
        }
        else
        {
            if section == 0
            {
                return 1
            }
            else
            {
                return 85
            }
        }
        
    }
    
    @objc func clickedPP(sender: UIButton)
    {
        if let url = URL(string: "https://docs.google.com/document/d/1k2Rtqw0BbPk0CawwkAvd_HjeNTx6vnWf19w_c9CltGQ/mobilebasic") {
            UIApplication.shared.open(url)
        }
        
    }
    
    @objc func clickedTC(sender: UIButton)
    {
        if let url = URL(string: "https://docs.google.com/document/d/1D7KLRNsUTUdvNGnPm4Lt7LOFYlMinVzO3aafqizK_t8/mobilebasic") {
            UIApplication.shared.open(url)
        }
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
}

struct AppUse {
    
    static let appID = "1447952480"
    
    static func rateApp(){
        if let url = URL(string:"itms-apps://itunes.apple.com/app/\(appID)") {
            AppUse.openURL(url)
        }
    }
    
    static func rateAppDeveloper(){
        if let url = URL(string:"itms-apps://itunes.apple.com/app/\(appID)") {
            AppUse.openURL(url)
        }
    }
    
    static func openURL(_ url: URL){
        if UIApplication.shared.canOpenURL(url){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    static let shareText = "ClickToChat - "
    
    static func shareApp(inController controller:UIViewController){
        let textToShare = "\(AppUse.shareText) \n itms-apps://itunes.apple.com/app/\(appID)"
        AppUse.itemShare(inController: controller, items: textToShare)
    }
    
    
    static func itemShare(inController controller:UIViewController, items:Any){
        let objectsToShare = [items]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = controller.view
        activityVC.popoverPresentationController?.sourceRect = CGRect(x: 100, y: 200, width: 300, height: 300)
        controller.present(activityVC, animated: true, completion: nil)
    }
    
}
