//
//  MessageListViewController.swift
//  ClickToChat
//
//  Created by Gabani King on 06/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class MessageListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblViewMessage: UITableView!
    
    var arrMessage = NSMutableArray()
    var delegate:MessageDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblViewMessage.delegate = self
        tblViewMessage.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if appDelegate.arrCurrentMessageList.count > 0
        {
            self.arrMessage = appDelegate.getCurrentMessageList()
            self.tblViewMessage.reloadData()
        }
        else
        {
            arrMessage = ["Test","Hello","Is it good time to talk?","I'm on my way..","My details: John smith, email: john@email.com, mobile: 1-122-555-2000","Can you send me details about the project?"]
            
            appDelegate.saveCurrentMessageList(array: arrMessage)
            appDelegate.arrCurrentMessageList = arrMessage
        }
        
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMessage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblViewMessage.dequeueReusableCell(withIdentifier: "MessageListTableCell") as! MessageListTableCell
        cell.lblMsg.text = arrMessage[indexPath.row] as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.onMessageReady(type: arrMessage[indexPath.row] as! String) // Delegate
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    @IBAction func clickedAdd(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewMessageVC") as! NewMessageVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let dicData = arrMessage[indexPath.row]
        
        let editAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: { (action, indexPath) in
            print("Delete tapped")
            
            let alert = UIAlertController(title: "Alert", message: "Are you sure, you want delete?", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            })
            alert.addAction(ok)
            let cancel = UIAlertAction(title: "Delete", style: .destructive, handler: { action in
                
                self.arrMessage.remove(dicData)
                self.tblViewMessage.reloadData()
                
                appDelegate.saveCurrentMessageList(array: self.arrMessage)
                appDelegate.arrCurrentMessageList = self.arrMessage
            })
            
            alert.addAction(cancel)
            self.present(alert, animated: true)
            
        })
        editAction.backgroundColor = UIColor.red
        
        let deleteAction = UITableViewRowAction(style: .default, title: "Edit", handler: { (action, indexPath) in
            print("Edit tapped")
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewMessageVC") as! NewMessageVC
            vc.isEditMessage = true
            vc.currentIndex = indexPath.row
            vc.strEditMessage = self.arrMessage[indexPath.row] as? String ?? ""
            self.navigationController?.pushViewController(vc, animated: false)
            
        })
        
        deleteAction.backgroundColor = App_Color_Blue
        
        return [editAction, deleteAction]
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
}
