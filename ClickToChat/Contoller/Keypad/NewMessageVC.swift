//
//  NewMessageVC.swift
//  ClickToChat
//
//  Created by Gabani King on 06/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class NewMessageVC: UIViewController {
    
    @IBOutlet weak var txtMesg: UITextView!
    
    var arrMessage = NSMutableArray()
    var arrMessage1 = NSMutableArray()
    
    var isEditMessage = false
    var currentIndex = 0
    var strEditMessage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if appDelegate.arrCurrentMessageList.count > 0
        {
            self.arrMessage1 = appDelegate.getCurrentMessageList()
        }
        
        if isEditMessage == true
        {
            self.txtMesg.text = strEditMessage
            self.arrMessage = appDelegate.getCurrentMessageList()
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func clickedAddMessage(_ sender: Any) {
        
        if self.txtMesg.text != "" {
            
            if isEditMessage == true
            {
                self.arrMessage.removeObject(at: currentIndex)
                self.arrMessage.insert(self.txtMesg.text ?? "", at: currentIndex)
            }
            else
            {
                self.arrMessage.removeAllObjects()
                self.arrMessage.add(self.txtMesg.text!)
                
                for obj in arrMessage1 {
                    self.arrMessage.add(obj)
                }
            }
            
            appDelegate.saveCurrentMessageList(array: arrMessage)
            appDelegate.arrCurrentMessageList = arrMessage
            
            self.navigationController?.popViewController(animated: false)
        }
        else
        {
            self.navigationController?.popViewController(animated: false)
        }
        
    }
    
}
