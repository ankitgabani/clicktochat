//
//  KeypadViewController.swift
//  ClickToChat
//
//  Created by Gabani King on 28/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import MarqueeLabel
import DialCountries
import PhoneNumberKit
import MessageUI
import GoogleMobileAds
import IQKeyboardManager

//MARK:- Delegate
protocol MessageDelegate
{
    func onMessageReady(type: String)
}

class KeypadViewController: UIViewController, MessageDelegate, MFMessageComposeViewControllerDelegate,GADFullScreenContentDelegate, GADBannerViewDelegate,UITextFieldDelegate {
    

    //MARK:- IBOutlet
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view6: UIView!
    
    @IBOutlet weak var view7: UIView!
    @IBOutlet weak var view8: UIView!
    @IBOutlet weak var view9: UIView!
    
    @IBOutlet weak var viewCopt: UIView!
    @IBOutlet weak var view0: UIView!
    @IBOutlet weak var viewRemove: UIView!
    
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var viewWh: UIView!
    @IBOutlet weak var viewShare1: UIView!
    
    
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!

    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var btnAddName: UIButton!
    @IBOutlet weak var btnAddMessage: UIButton!
    @IBOutlet weak var lblNameMarq: MarqueeLabel!
    @IBOutlet weak var viewAddedName: UIView!
    
    @IBOutlet weak var lblMessgeMarq: MarqueeLabel!
    @IBOutlet weak var viewAddedMessage: UIView!
    
    var dicKeypadDetails = NSMutableDictionary()
    var arrKeypadDetails = NSMutableArray()
    
    var arrKeypadDetailszz: [CCKeypadNumber] = [CCKeypadNumber]()
    var dicKeypadDetailszz = CCKeypadNumber()
    
    var interstitial: GADInterstitialAd!
    let customView = UIView()
    
    var strSelected = 1
    
    var strCurrentCountryCode = "+91"
    
    //MARK:- View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            self.view1.layer.cornerRadius = self.view1.frame.height / 2
            self.view1.clipsToBounds = true
            
            self.view2.layer.cornerRadius = self.view2.frame.height / 2
            self.view2.clipsToBounds = true
            
            self.view3.layer.cornerRadius = self.view3.frame.height / 2
            self.view3.clipsToBounds = true
            
            self.view4.layer.cornerRadius = self.view4.frame.height / 2
            self.view4.clipsToBounds = true
            
            self.view5.layer.cornerRadius = self.view5.frame.height / 2
            self.view5.clipsToBounds = true
            
            self.view6.layer.cornerRadius = self.view6.frame.height / 2
            self.view6.clipsToBounds = true
            
            self.view7.layer.cornerRadius = self.view7.frame.height / 2
            self.view7.clipsToBounds = true
            
            self.view8.layer.cornerRadius = self.view8.frame.height / 2
            self.view8.clipsToBounds = true
            
            self.view9.layer.cornerRadius = self.view9.frame.height / 2
            self.view9.clipsToBounds = true
            
            self.view0.layer.cornerRadius = self.view0.frame.height / 2
            self.view0.clipsToBounds = true
            
            self.viewCopt.layer.cornerRadius = self.viewCopt.frame.height / 2
            self.viewCopt.clipsToBounds = true
            
            self.viewRemove.layer.cornerRadius = self.viewRemove.frame.height / 2
            self.viewRemove.clipsToBounds = true
            
            self.viewMessage.layer.cornerRadius = self.viewMessage.frame.height / 2
            self.viewMessage.clipsToBounds = true
            
            self.viewWh.layer.cornerRadius = self.viewWh.frame.height / 2
            self.viewWh.clipsToBounds = true
            
            self.viewShare1.layer.cornerRadius = self.viewShare1.frame.height / 2
            self.viewShare1.clipsToBounds = true
        }
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                topConstraint.constant = 10
                bottomConstraint.constant = 10
                
            case 1334:
                print("iPhone 6/6S/7/8")
                topConstraint.constant = 20
                bottomConstraint.constant = 20

            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                topConstraint.constant = 30
                bottomConstraint.constant = 30
                
            case 2436:
                print("iPhone X/XS/11 Pro")
                topConstraint.constant = 50
                bottomConstraint.constant = 50

            case 2688:
                topConstraint.constant = 50
                bottomConstraint.constant = 50

            case 1792:
                print("iPhone XR/ 11 ")
                topConstraint.constant = 50
                bottomConstraint.constant = 50

            default:
                print("Unknown")
                topConstraint.constant = 50
                bottomConstraint.constant = 50

            }
        }
        
        
        guard let regionCode = NSLocale.current.regionCode,
              let callingCode = GetCountryCallingCode(countryRegionCode: regionCode )
        else { return }
        
        self.strCurrentCountryCode = "+\(callingCode)"
        
        print("Region: \(regionCode) Country calling code is \(callingCode)")
        
        if appDelegate.isUpdateKeypad == true
        {
            let dicData = appDelegate.dicKeypadNumber
            
            let phone = dicData.userMobile ?? ""
            let name = dicData.UserName ?? ""
            let message = dicData.UserMessage ?? ""
            let type = dicData.UserType ?? ""
            let UserCode = dicData.UserCode ?? ""
            
            self.lblCode.text = UserCode
            self.txtPhoneNumber.text = phone
            
            if message != "" {
                DispatchQueue.main.async {
                    self.btnAddMessage.isHidden = true
                    self.viewAddedMessage.isHidden = false
                    self.lblMessgeMarq.text = message
                }
            }
            else
            {
                self.btnAddMessage.isHidden = false
                viewAddedMessage.isHidden = true
                lblMessgeMarq.text = ""
            }
            
            if name != "" {
                self.btnAddName.isHidden = true
                self.viewAddedName.isHidden = false
                self.lblNameMarq.text = name
            }
            else
            {
                self.btnAddName.isHidden = false
                viewAddedName.isHidden = true
                lblNameMarq.text = ""
            }
            
        }
        else
        {
            self.lblCode.text = strCurrentCountryCode
            
            self.btnAddName.isHidden = false
            self.btnAddMessage.isHidden = false
            
            viewAddedMessage.isHidden = true
            lblMessgeMarq.text = ""
            
            viewAddedName.isHidden = true
            lblNameMarq.text = ""
        }
        
        txtPhoneNumber.delegate = self
        txtPhoneNumber.inputView = customView
        
        customView.isHidden = true
        
        lblNameMarq.type = .continuous
        lblNameMarq.textAlignment = .right
        lblNameMarq.lineBreakMode = .byTruncatingHead
        lblNameMarq.speed = .duration(7.0)
        //  lblNameMarq.fadeLength = 15.0
        lblNameMarq.leadingBuffer = 10.0
        
        lblMessgeMarq.type = .continuous
        lblMessgeMarq.textAlignment = .right
        lblMessgeMarq.lineBreakMode = .byTruncatingHead
        lblMessgeMarq.speed = .duration(7.0)
        //  lblNameMarq.fadeLength = 15.0
        lblMessgeMarq.leadingBuffer = 10.0
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        txtPhoneNumber.becomeFirstResponder()
        
        IQKeyboardManager.shared().isEnabled = false
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        txtPhoneNumber.autocorrectionType = .no
        txtPhoneNumber.hideSuggestions()
        
        IQKeyboardManager.shared().shouldResignOnTouchOutside = false

         let request = GADRequest()
        GADInterstitialAd.load(withAdUnitID:interstitial_Ads_ID,request: request,
                               completionHandler: { [self] ad, error in
                                if let error = error {
                                    return
                                }
                                interstitial = ad
                                interstitial?.fullScreenContentDelegate = self
                               }
        )
        
    }
    
    //MARK:- Action Method
    public func alertWithTextField(title: String? = nil, message: String? = nil, placeholder: String? = nil, completion: @escaping ((String) -> Void) = { _ in }) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addTextField() { newTextField in
            newTextField.placeholder = placeholder
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in completion("") })
        alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
            if
                let textFields = alert.textFields,
                let tf = textFields.first,
                let result = tf.text
            
            
            { completion(result)
                self.btnAddName.isHidden = true
                self.viewAddedName.isHidden = false
                self.lblNameMarq.text = result
            }
            else
            { completion("") }
        })
        navigationController?.present(alert, animated: true)
    }
    
    @IBAction func clickedChooseCode(_ sender: Any) {
        let controller = DialCountriesController(locale: Locale(identifier: "ae"))
        controller.delegate = self
        controller.show(vc: self)
    }
    
    @IBAction func clickedEnterType(_ sender: UIButton) {
      
        
        if sender.tag == 22
        {
            let removedLast = self.txtPhoneNumber.text?.dropLast(1)
            
            if removedLast != "" {
                self.txtPhoneNumber.text = "\(String(removedLast!))"
            }
            else
            {
                self.txtPhoneNumber.text = ""
            }
        }
        else if sender.tag == 11
        {

        }
        else
        {
            self.txtPhoneNumber.text = "\(self.txtPhoneNumber.text!)\(sender.tag)"
        }
        
    }
    
    @IBAction func clickedSendMessage(_ sender: Any) {
        dicKeypadDetailszz = CCKeypadNumber()
        
        strSelected = 1
        
        let phoneNumberKit = PhoneNumberKit()
        
        do {
            
            let phoneNumber = try phoneNumberKit.parse("\(self.lblCode.text!)\(self.txtPhoneNumber.text!)")
            
            if interstitial != nil {
                interstitial.present(fromRootViewController: self)
            } else {
                print("Ad wasn't ready")
                
                dicKeypadDetailszz.UserCode = self.lblCode.text!
                dicKeypadDetailszz.UserMessage = self.lblMessgeMarq.text!
                dicKeypadDetailszz.UserName = self.lblNameMarq.text!
                dicKeypadDetailszz.UserType = "Text Message"
                dicKeypadDetailszz.userMobile = self.txtPhoneNumber.text
                
                appDelegate.arrKeypadNumber.append(dicKeypadDetailszz)
                appDelegate.saveKeypadNumberList(array: appDelegate.arrKeypadNumber)
                
                let messageVC = MFMessageComposeViewController()
                messageVC.body = self.lblMessgeMarq.text ?? "";
                messageVC.recipients = ["\(self.lblCode.text!)\(self.txtPhoneNumber.text!)"]
                messageVC.messageComposeDelegate = self
                self.present(messageVC, animated: true, completion: nil)
            }
            
        }
        catch {
            print("Generic parser error")
            let alert = UIAlertController(title: "", message: "Please enter valid mobile number", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    @IBAction func clickedSendWhatsapp(_ sender: Any) {
        
        dicKeypadDetailszz = CCKeypadNumber()
        
        strSelected = 2
        
        let phoneNumberKit = PhoneNumberKit()
        
        do {
            
            let phoneNumber = try phoneNumberKit.parse("\(self.lblCode.text!)\(self.txtPhoneNumber.text!)")
            
            if interstitial != nil {
                interstitial.present(fromRootViewController: self)
            } else {
                print("Ad wasn't ready")
                
                let urlWhats = "whatsapp://send?phone=\("\(self.lblCode.text!)\(self.txtPhoneNumber.text!)")&text=\(self.lblMessgeMarq.text ?? "")"
                if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                    if let whatsappURL = NSURL(string: urlString) {
                        if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                            UIApplication.shared.open(whatsappURL as URL)
                            
                            dicKeypadDetailszz.UserCode = self.lblCode.text!
                            dicKeypadDetailszz.UserMessage = self.lblMessgeMarq.text!
                            dicKeypadDetailszz.UserName = self.lblNameMarq.text!
                            dicKeypadDetailszz.UserType = "WhatsApp"
                            dicKeypadDetailszz.userMobile = self.txtPhoneNumber.text
                            
                            appDelegate.arrKeypadNumber.append(dicKeypadDetailszz)
                            appDelegate.saveKeypadNumberList(array: appDelegate.arrKeypadNumber)
                            
                        }
                        else {
                            print("please install watsapp")
                        }
                    }
                }
            }
            
            
        }
        catch {
            print("Generic parser error")
            let alert = UIAlertController(title: "", message: "Please enter valid mobile number", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func clickedShareMesg(_ sender: Any) {
        strSelected = 3
        
        let phoneNumberKit = PhoneNumberKit()
        
        do {
            
            let phoneNumber = try phoneNumberKit.parse("\(self.lblCode.text!)\(self.txtPhoneNumber.text!)")
            
            if interstitial != nil {
                interstitial.present(fromRootViewController: self)
            } else {
                print("Ad wasn't ready")
                let vc = UIActivityViewController(activityItems: ["https://api.whatsapp.com/send?phone=\("\(self.lblCode.text!)\(self.txtPhoneNumber.text!)")&text="], applicationActivities: nil)
                if UIDevice.current.userInterfaceIdiom == .pad {
                    if let popoverController = vc.popoverPresentationController {
                        let btnShare = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                        btnShare.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height - 50)
                        let barButton = UIBarButtonItem(customView: btnShare)
                        popoverController.barButtonItem = barButton
                    }
                }
                present(vc, animated: true)
            }
            
        }
        catch {
            print("Generic parser error")
            let alert = UIAlertController(title: "", message: "Please enter valid mobile number", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
        case .failed:
            print("Message failed")
        case .sent:
            print("Message was sent")
        default:
            return
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedAddName(_ sender: Any) {
        alertWithTextField(title: "Add Name", message: "Enter a name of the person", placeholder: "Name") { result in
            print(result)
        }
    }
    
    @IBAction func btnAddMessage(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MessageListViewController") as! MessageListViewController
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btndeleteName(_ sender: Any) {
        self.btnAddName.isHidden = false
        self.viewAddedName.isHidden = true
        self.lblNameMarq.text = ""
    }
    
    @IBAction func btndeleteMessage(_ sender: Any) {
        self.btnAddMessage.isHidden = false
        self.viewAddedMessage.isHidden = true
        self.lblMessgeMarq.text = ""
    }
    
    
    @IBAction func clickedEnterNumber(_ sender: Any) {
        txtPhoneNumber.becomeFirstResponder()
    }
    
    //MARK:- Delegate
    func onMessageReady(type: String)
    {
        self.btnAddMessage.isHidden = true
        self.viewAddedMessage.isHidden = false
        self.lblMessgeMarq.text = type
    }
    
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error)
    {
        dicKeypadDetailszz = CCKeypadNumber()
        
        if strSelected == 1
        {
            
            dicKeypadDetailszz.UserCode = self.lblCode.text!
            dicKeypadDetailszz.UserMessage = self.lblMessgeMarq.text!
            dicKeypadDetailszz.UserName = self.lblNameMarq.text!
            dicKeypadDetailszz.UserType = "Text Message"
            dicKeypadDetailszz.userMobile = self.txtPhoneNumber.text
            
            appDelegate.arrKeypadNumber.append(dicKeypadDetailszz)
            appDelegate.saveKeypadNumberList(array: appDelegate.arrKeypadNumber)
            
            print("Send *********** \(appDelegate.arrKeypadNumber.count) *********")
            
            print("Send Conunt *********** \(appDelegate.getKeypadNumberList().count) *********")
            
            let messageVC = MFMessageComposeViewController()
            messageVC.body = self.lblMessgeMarq.text ?? "";
            messageVC.recipients = ["\(self.lblCode.text!)\(self.txtPhoneNumber.text!)"]
            messageVC.messageComposeDelegate = self
            self.present(messageVC, animated: true, completion: nil)
        }
        else if strSelected == 2
        {
            
            let urlWhats = "whatsapp://send?phone=\("\(self.lblCode.text!)\(self.txtPhoneNumber.text!)")&text=\(self.lblMessgeMarq.text ?? "")"
            if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                if let whatsappURL = NSURL(string: urlString) {
                    if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                        UIApplication.shared.open(whatsappURL as URL)
                        
                        dicKeypadDetailszz.UserCode = self.lblCode.text!
                        dicKeypadDetailszz.UserMessage = self.lblMessgeMarq.text!
                        dicKeypadDetailszz.UserName = self.lblNameMarq.text!
                        dicKeypadDetailszz.UserType = "WhatsApp"
                        dicKeypadDetailszz.userMobile = self.txtPhoneNumber.text
                        
                        appDelegate.arrKeypadNumber.append(dicKeypadDetailszz)
                        appDelegate.saveKeypadNumberList(array: appDelegate.arrKeypadNumber)
                        
                        print("Send *********** \(appDelegate.arrKeypadNumber.count) *********")
                        print("Send Conunt *********** \(appDelegate.getKeypadNumberList().count) *********")
                    }
                    else {
                        print("please install watsapp")
                    }
                }
            }
        }
        else
        {
            
            let vc = UIActivityViewController(activityItems: ["https://api.whatsapp.com/send?phone=\("\(self.lblCode.text!)\(self.txtPhoneNumber.text!)")&text="], applicationActivities: nil)
            if UIDevice.current.userInterfaceIdiom == .pad {
                if let popoverController = vc.popoverPresentationController {
                    let btnShare = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                    btnShare.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height - 50)
                    let barButton = UIBarButtonItem(customView: btnShare)
                    popoverController.barButtonItem = barButton
                }
            }
            present(vc, animated: true)
        }
    }
    
    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd)
    {
        
    }
    
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd)
    {
        dicKeypadDetailszz = CCKeypadNumber()
        
        if strSelected == 1
        {
            
            dicKeypadDetailszz.UserCode = self.lblCode.text!
            dicKeypadDetailszz.UserMessage = self.lblMessgeMarq.text!
            dicKeypadDetailszz.UserName = self.lblNameMarq.text!
            dicKeypadDetailszz.UserType = "Text Message"
            dicKeypadDetailszz.userMobile = self.txtPhoneNumber.text
            
            appDelegate.arrKeypadNumber.append(dicKeypadDetailszz)
            appDelegate.saveKeypadNumberList(array: appDelegate.arrKeypadNumber)
            
            print("Send *********** \(appDelegate.arrKeypadNumber.count) *********")
            
            print("Send Conunt *********** \(appDelegate.getKeypadNumberList().count) *********")
            
            let messageVC = MFMessageComposeViewController()
            messageVC.body = self.lblMessgeMarq.text ?? "";
            messageVC.recipients = ["\(self.lblCode.text!)\(self.txtPhoneNumber.text!)"]
            messageVC.messageComposeDelegate = self
            self.present(messageVC, animated: true, completion: nil)
        }
        else if strSelected == 2
        {
            
            let urlWhats = "whatsapp://send?phone=\("\(self.lblCode.text!)\(self.txtPhoneNumber.text!)")&text=\(self.lblMessgeMarq.text ?? "")"
            if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                if let whatsappURL = NSURL(string: urlString) {
                    if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                        UIApplication.shared.open(whatsappURL as URL)
                        
                        dicKeypadDetailszz.UserCode = self.lblCode.text!
                        dicKeypadDetailszz.UserMessage = self.lblMessgeMarq.text!
                        dicKeypadDetailszz.UserName = self.lblNameMarq.text!
                        dicKeypadDetailszz.UserType = "WhatsApp"
                        dicKeypadDetailszz.userMobile = self.txtPhoneNumber.text
                        
                        appDelegate.arrKeypadNumber.append(dicKeypadDetailszz)
                        appDelegate.saveKeypadNumberList(array: appDelegate.arrKeypadNumber)
                        
                        print("Send *********** \(appDelegate.arrKeypadNumber.count) *********")
                        print("Send Conunt *********** \(appDelegate.getKeypadNumberList().count) *********")
                    }
                    else {
                        print("please install watsapp")
                    }
                }
            }
        }
        else
        {
            
            let vc = UIActivityViewController(activityItems: ["https://api.whatsapp.com/send?phone=\("\(self.lblCode.text!)\(self.txtPhoneNumber.text!)")&text="], applicationActivities: nil)
            if UIDevice.current.userInterfaceIdiom == .pad {
                if let popoverController = vc.popoverPresentationController {
                    let btnShare = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                    btnShare.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height - 50)
                    let barButton = UIBarButtonItem(customView: btnShare)
                    popoverController.barButtonItem = barButton
                }
            }
            present(vc, animated: true)
        }
    }
    
    func GetCountryCallingCode(countryRegionCode:String) -> String? {
        
        let prefixCodes = ["AF": "93", "AE": "971", "AL": "355", "AN": "599", "AS":"1", "AD": "376", "AO": "244", "AI": "1", "AG":"1", "AR": "54","AM": "374", "AW": "297", "AU":"61", "AT": "43","AZ": "994", "BS": "1", "BH":"973", "BF": "226","BI": "257", "BD": "880", "BB": "1", "BY": "375", "BE":"32","BZ": "501", "BJ": "229", "BM": "1", "BT":"975", "BA": "387", "BW": "267", "BR": "55", "BG": "359", "BO": "591", "BL": "590", "BN": "673", "CC": "61", "CD":"243","CI": "225", "KH":"855", "CM": "237", "CA": "1", "CV": "238", "KY":"345", "CF":"236", "CH": "41", "CL": "56", "CN":"86","CX": "61", "CO": "57", "KM": "269", "CG":"242", "CK": "682", "CR": "506", "CU":"53", "CY":"537","CZ": "420", "DE": "49", "DK": "45", "DJ":"253", "DM": "1", "DO": "1", "DZ": "213", "EC": "593", "EG":"20", "ER": "291", "EE":"372","ES": "34", "ET": "251", "FM": "691", "FK": "500", "FO": "298", "FJ": "679", "FI":"358", "FR": "33", "GB":"44", "GF": "594", "GA":"241", "GS": "500", "GM":"220", "GE":"995","GH":"233", "GI": "350", "GQ": "240", "GR": "30", "GG": "44", "GL": "299", "GD":"1", "GP": "590", "GU": "1", "GT": "502", "GN":"224","GW": "245", "GY": "595", "HT": "509", "HR": "385", "HN":"504", "HU": "36", "HK": "852", "IR": "98", "IM": "44", "IL": "972", "IO":"246", "IS": "354", "IN": "91", "ID":"62", "IQ":"964", "IE": "353","IT":"39", "JM":"1", "JP": "81", "JO": "962", "JE":"44", "KP": "850", "KR": "82","KZ":"77", "KE": "254", "KI": "686", "KW": "965", "KG":"996","KN":"1", "LC": "1", "LV": "371", "LB": "961", "LK":"94", "LS": "266", "LR":"231", "LI": "423", "LT": "370", "LU": "352", "LA": "856", "LY":"218", "MO": "853", "MK": "389", "MG":"261", "MW": "265", "MY": "60","MV": "960", "ML":"223", "MT": "356", "MH": "692", "MQ": "596", "MR":"222", "MU": "230", "MX": "52","MC": "377", "MN": "976", "ME": "382", "MP": "1", "MS": "1", "MA":"212", "MM": "95", "MF": "590", "MD":"373", "MZ": "258", "NA":"264", "NR":"674", "NP":"977", "NL": "31","NC": "687", "NZ":"64", "NI": "505", "NE": "227", "NG": "234", "NU":"683", "NF": "672", "NO": "47","OM": "968", "PK": "92", "PM": "508", "PW": "680", "PF": "689", "PA": "507", "PG":"675", "PY": "595", "PE": "51", "PH": "63", "PL":"48", "PN": "872","PT": "351", "PR": "1","PS": "970", "QA": "974", "RO":"40", "RE":"262", "RS": "381", "RU": "7", "RW": "250", "SM": "378", "SA":"966", "SN": "221", "SC": "248", "SL":"232","SG": "65", "SK": "421", "SI": "386", "SB":"677", "SH": "290", "SD": "249", "SR": "597","SZ": "268", "SE":"46", "SV": "503", "ST": "239","SO": "252", "SJ": "47", "SY":"963", "TW": "886", "TZ": "255", "TL": "670", "TD": "235", "TJ": "992", "TH": "66", "TG":"228", "TK": "690", "TO": "676", "TT": "1", "TN":"216","TR": "90", "TM": "993", "TC": "1", "TV":"688", "UG": "256", "UA": "380", "US": "1", "UY": "598","UZ": "998", "VA":"379", "VE":"58", "VN": "84", "VG": "1", "VI": "1","VC":"1", "VU":"678", "WS": "685", "WF": "681", "YE": "967", "YT": "262","ZA": "27" , "ZM": "260", "ZW":"263"]
        let countryDialingCode = prefixCodes[countryRegionCode]
        return countryDialingCode
        
    }
    
}

extension KeypadViewController : DialCountriesControllerDelegate {
    func didSelected(with country: Country) {
        //        JSN.log("selected country ===>%@", country.dialCode)
        //        JSN.log("selected country flag ===>%@", country.flag)
        //        self.countryCode.text = country.flag + " " + (country.dialCode ?? "+91")
        
        self.lblCode.text = country.dialCode ?? "+91"
    }
    
}



extension UITextView {
    func hideSuggestions() {
        // Removes suggestions only
        autocorrectionType = .no
        //Removes Undo, Redo, Copy & Paste options
        removeUndoRedoOptions()
    }
}

extension UITextField {
    func hideSuggestions() {
        // Removes suggestions only
        autocorrectionType = .no
        //Removes Undo, Redo, Copy & Paste options
        removeUndoRedoOptions()
    }
}

extension UIResponder {
    func removeUndoRedoOptions() {
        //Removes Undo, Redo, Copy & Paste options
        inputAssistantItem.leadingBarButtonGroups = []
        inputAssistantItem.trailingBarButtonGroups = []
    }
}

