//
//  MessageListTableCell.swift
//  ClickToChat
//
//  Created by Gabani King on 06/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class MessageListTableCell: UITableViewCell {
    
    @IBOutlet weak var lblMsg: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
