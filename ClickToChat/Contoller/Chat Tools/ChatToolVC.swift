//
//  ChatToolVC.swift
//  ClickToChat
//
//  Created by Gabani King on 28/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import GoogleMobileAds
class ChatToolVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, GADFullScreenContentDelegate, GADBannerViewDelegate {
    
    //MARK:- IBOutlet
    @IBOutlet weak var collectionViewTools: UICollectionView!
    
    var interstitial: GADInterstitialAd!

    var arrName = ["Stickers","Jokes","Quotes","Character","Dual WA"]
    var arrIamge = ["1","2","3","4","5"]
    
    var arrColor = [UIColor(red: 254/255, green: 228/255, blue: 225/255, alpha: 1),UIColor(red: 230/255, green: 245/255, blue: 158/255, alpha: 1),UIColor(red: 181/255, green: 182/255, blue: 220/255, alpha: 1),UIColor(red: 167/255, green: 250/255, blue: 253/255, alpha: 1),UIColor(red: 253/255, green: 205/255, blue: 126/255, alpha: 1)]
    
    let sectionInsets = UIEdgeInsets(top: 40.0,
                                     left: 15.0,
                                     bottom: 15.0,
                                     right: 15.0)
    let itemsPerRow: CGFloat = 2
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: widthPerItem)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 40.0, left: 15.0, bottom: 15.0, right: 15.0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 8.0
        return _flowLayout
    }
    
    //MARK:- View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        collectionViewTools.delegate = self
        collectionViewTools.dataSource = self
        
        collectionViewTools.collectionViewLayout = flowLayout
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let request = GADRequest()
        GADInterstitialAd.load(withAdUnitID:interstitial_Ads_ID,request: request,
                               completionHandler: { [self] ad, error in
                                if let error = error {
                                    return
                                }
                                interstitial = ad
                                interstitial?.fullScreenContentDelegate = self
                               }
        )
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK:- UICollectionView Delegate & DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionViewTools.dequeueReusableCell(withReuseIdentifier: "ChatToolsCell", for: indexPath) as! ChatToolsCell
        
        cell.lblANme.text = arrName[indexPath.row]
        cell.imgPic.image = UIImage(named: arrIamge[indexPath.row])
        cell.mainView.backgroundColor = arrColor[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if interstitial != nil {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
        
        if indexPath.row == 0
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "StickerPackVC") as! StickerPackVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else if indexPath.row == 1
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "JokesViewController") as! JokesViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 2
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "QuotesVC") as! QuotesVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 3
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SpecialCharViewController") as! SpecialCharViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DualWAViewController") as! DualWAViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK:- GADFullScreenContent Delegate
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error)
    {
        
    }
    
    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd)
    {
        
    }
    
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd)
    {
        
    }
}
