//
//  StickerListCollectionCell.swift
//  ClickToChat
//
//  Created by Gabani King on 30/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class StickerListCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgSticker: UIImageView!
}
