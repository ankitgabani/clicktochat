//
//  QuotesTableCell.swift
//  ClickToChat
//
//  Created by Gabani King on 30/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class QuotesTableCell: UITableViewCell {

    @IBOutlet weak var lblJokes: UILabel!
    @IBOutlet weak var lblType: UILabel!
    
    @IBOutlet weak var btnCopy: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var viewRound: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            self.viewRound.layer.cornerRadius = self.viewRound.frame.height / 2
            self.viewRound.clipsToBounds = true
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
