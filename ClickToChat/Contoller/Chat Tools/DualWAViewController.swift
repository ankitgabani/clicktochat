//
//  DualWAViewController.swift
//  ClickToChat
//
//  Created by Gabani King on 30/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD

class DualWAViewController: UIViewController, WKNavigationDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var mainView: UIView!
    
    var webView : WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        webView = WKWebView()
        DispatchQueue.main.async {
            self.webView.frame = CGRect.init(x: 0, y: 0, width: self.mainView.frame.size.width, height: self.mainView.frame.size.height)
            
            self.webView.load(NSURLRequest(url: NSURL(string: "https://www.google.com/")! as URL) as URLRequest)
            
            self.webView.allowsBackForwardNavigationGestures = true
            
            self.webView.navigationDelegate = self
            
            self.webView.scrollView.bounces = false
            
            self.mainView.addSubview(self.webView)
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        SVProgressHUD.show()
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SVProgressHUD.dismiss()
        
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(error)
        
    }
    @IBAction func clickedRefresh(_ sender: Any) {
        DispatchQueue.main.async {
            
            self.webView.load(NSURLRequest(url: NSURL(string: "https://www.google.com/")! as URL) as URLRequest)
            
        }
    }
    
}
