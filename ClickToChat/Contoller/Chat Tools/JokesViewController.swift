//
//  JokesViewController.swift
//  ClickToChat
//
//  Created by Gabani King on 30/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import GoogleMobileAds

class JokesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, GADFullScreenContentDelegate, GADBannerViewDelegate  {
    
    @IBOutlet weak var tblView: UITableView!
    var interstitial: GADInterstitialAd!
    var arrJokes: [CCJokesDATA] = [CCJokesDATA]()
    
    var isCopy = true
    var objCurrentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let request = GADRequest()
        GADInterstitialAd.load(withAdUnitID:interstitial_Ads_ID,request: request,
                               completionHandler: { [self] ad, error in
                                if let error = error {
                                    return
                                }
                                interstitial = ad
                                interstitial?.fullScreenContentDelegate = self
                               }
        )
        
        tblView.delegate = self
        tblView.dataSource = self
        
        callJoksAPI()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrJokes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "JokesTableCell") as! JokesTableCell
        
        let dicData = arrJokes[indexPath.row]
        
        cell.lblJokes.text = dicData.text ?? ""
        
        cell.lblType.text = dicData.author ?? ""
        
        cell.btnCopy.backgroundColor = App_Color_Blue
        cell.btnShare.backgroundColor = App_Color_Blue
        
        cell.btnShare.tag = indexPath.row
        cell.btnShare.addTarget(self, action: #selector(clickedShare(sender:)), for: .touchUpInside)
        
        cell.btnCopy.tag = indexPath.row
        cell.btnCopy.addTarget(self, action: #selector(clickedCopy(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func clickedCopy(sender: UIButton) {
        isCopy = true
        objCurrentIndex = sender.tag
        
        if interstitial != nil {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
            
            let dicData = arrJokes[sender.tag]
            
            let alert = UIAlertController(title: "", message: "Text copied.", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                
                UIPasteboard.general.string = dicData.text ?? ""
            })
            alert.addAction(ok)
            
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }
        
    }
    
    @objc func clickedShare(sender: UIButton) {
        isCopy = false
        objCurrentIndex = sender.tag
        
        if interstitial != nil {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
            
            let dicData = arrJokes[sender.tag]
            
            let vc = UIActivityViewController(activityItems: [dicData.text ?? ""], applicationActivities: nil)
            if UIDevice.current.userInterfaceIdiom == .pad {
                if let popoverController = vc.popoverPresentationController {
                    let btnShare = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                    btnShare.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height - 50)
                    let barButton = UIBarButtonItem(customView: btnShare)
                    popoverController.barButtonItem = barButton
                }
            }
            self.present(vc, animated: true)
        }
        
        
    }
    
    
    // MARK: - API Call
    func callJoksAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(GET_JOKES, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        let arrData = responseUser.value(forKey: "DATA") as? NSArray
                        
                        for obj in arrData! {
                            let dicData = CCJokesDATA(fromDictionary: (obj as? NSDictionary)!)
                            self.arrJokes.append(dicData)
                        }
                        
                        self.tblView.reloadData()
                        
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error)
    {
        
        if isCopy == true
        {
            
            let dicData = arrJokes[objCurrentIndex]
            
            let alert = UIAlertController(title: "", message: "Text copied.", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                
                UIPasteboard.general.string = dicData.text ?? ""
            })
            alert.addAction(ok)
            
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }
        else
        {
            let dicData = arrJokes[objCurrentIndex]
            
            let vc = UIActivityViewController(activityItems: [dicData.text ?? ""], applicationActivities: nil)
            if UIDevice.current.userInterfaceIdiom == .pad {
                if let popoverController = vc.popoverPresentationController {
                    let btnShare = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                    btnShare.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height - 50)
                    let barButton = UIBarButtonItem(customView: btnShare)
                    popoverController.barButtonItem = barButton
                }
            }
            self.present(vc, animated: true)
        }
    }
    
    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd)
    {
        
    }
    
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd)
    {
        if isCopy == true
        {
            
            let dicData = arrJokes[objCurrentIndex]
            
            let alert = UIAlertController(title: "", message: "Text copied.", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                
                UIPasteboard.general.string = dicData.text ?? ""
            })
            alert.addAction(ok)
            
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }
        else
        {
            let dicData = arrJokes[objCurrentIndex]
            
            let vc = UIActivityViewController(activityItems: [dicData.text ?? ""], applicationActivities: nil)
            if UIDevice.current.userInterfaceIdiom == .pad {
                if let popoverController = vc.popoverPresentationController {
                    let btnShare = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                    btnShare.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height - 50)
                    let barButton = UIBarButtonItem(customView: btnShare)
                    popoverController.barButtonItem = barButton
                }
            }
            self.present(vc, animated: true)
        }
        
    }
    
}
