//
//  StickerListVC.swift
//  ClickToChat
//
//  Created by Gabani King on 30/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import SDWebImage

class StickerListVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    //MARK:- IBOutlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewAddToWhatsapp: UIView!
    @IBOutlet weak var collectionViewSticker: UICollectionView!
    
    let sectionInsets = UIEdgeInsets(top: 10.0,
                                     left: 10.0,
                                     bottom: 10.0,
                                     right: 10.0)
    let itemsPerRow: CGFloat = 4
    
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: widthPerItem)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 10.0
        return _flowLayout
    }
        
    var arrStickerPack: [Sticker] = []
    var stickerPacks123: [StickerPack] = []
    var dicStickerObject = NSDictionary()
    
    
    //MARK:- View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = dicStickerObject.value(forKey: "name") as? String
        
        collectionViewSticker.delegate = self
        collectionViewSticker.dataSource = self
        
        collectionViewSticker.collectionViewLayout = flowLayout
        viewAddToWhatsapp.backgroundColor = App_Color_Blue
        // Do any additional setup after loading the view.
        
        let loadingAlert = UIAlertController(title: "Loading sticker packs", message: "\n\n", preferredStyle: .alert)
        loadingAlert.addSpinner()
        present(loadingAlert, animated: true)
        
        do {
            try StickerPackManager.fetchStickerPacks(fromJSON: dicStickerObject as! [String : Any]) { stickerPacks in
                self.navigationController?.navigationBar.alpha = 1.0
                
                loadingAlert.dismiss(animated: false) {
                    if stickerPacks.count > 0 {
                        self.stickerPacks123 = stickerPacks
                        self.arrStickerPack = self.stickerPacks123.first!.stickers
                        self.collectionViewSticker.reloadData()
                    }
                }
            }
        } catch StickerPackError.fileNotFound {
            fatalError("sticker_packs.wasticker not found.")
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
    
    //MARK:- Action Method
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedAddToWhatsapp(_ sender: UIButton) {
        
        if self.arrStickerPack.count > 0{
            
            let loadingAlert: UIAlertController = UIAlertController(title: "Sending to WhatsApp", message: "\n\n", preferredStyle: .alert)
            loadingAlert.addSpinner()
            present(loadingAlert, animated: true)
            
            stickerPacks123.first!.sendToWhatsApp { completed in
                loadingAlert.dismiss(animated: true)
            }
        }
        else{
            let alert = UIAlertController(title: "ClickToChat", message: "Please waiting..", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    
    //MARK:- UICollectionView Method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrStickerPack.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionViewSticker.dequeueReusableCell(withReuseIdentifier: "StickerListCollectionCell", for: indexPath) as! StickerListCollectionCell
        
        let dicData = self.arrStickerPack[indexPath.item]
        
        cell.imgSticker.image = dicData.imageData.image
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let sticker: Sticker = (self.arrStickerPack[indexPath.item])
        let cell = collectionViewSticker.cellForItem(at: indexPath) as? StickerListCollectionCell
        showActionSheet(withSticker: sticker, overCell: cell!)
        
    }
    
    // MARK: Targets
    
    func showActionSheet(withSticker sticker: Sticker, overCell cell: UICollectionViewCell) {
        var emojisString: String?
        
        #if DEBUG
        if let emojis = sticker.emojis {
            emojisString = emojis.joined(separator: " ")
        }
        #endif
        
        let actionSheet: UIAlertController = UIAlertController(title: "\n\n\n\n\n\n\n", message: emojisString, preferredStyle: .actionSheet)
        
        actionSheet.popoverPresentationController?.sourceView = cell.contentView
        actionSheet.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        actionSheet.popoverPresentationController?.sourceRect = CGRect(x: cell.contentView.bounds.midX, y: cell.contentView.bounds.midY, width: 0, height: 0)
        
        actionSheet.addAction(UIAlertAction(title: "Copy to Clipboard", style: .default, handler: { _ in
            sticker.copyToPasteboardAsImage()
        }))
        actionSheet.addAction(UIAlertAction(title: "Share via", style: .default, handler: { _ in
            self.showShareSheet(withSticker: sticker.imageData.image!)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        
        if let stickerImage = sticker.imageData.image {
            actionSheet.addImageView(withImage: stickerImage, animated: sticker.imageData.animated)
        }
        present(actionSheet, animated: true)
    }
    
    func showShareSheet(withSticker sticker: UIImage) {
        let image = sticker
        
        let shareViewController: UIActivityViewController = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        shareViewController.popoverPresentationController?.sourceView = self.view
        shareViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        shareViewController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        present(shareViewController, animated: true)
    }
    
    func copyToPasteboardAsImage(img: UIImageView) {
        if let image = img.image {
            Interoperability.copyImageToPasteboard(image: image)
        }
    }
    
}


extension UIAlertController {
    func addSpinner() {
        let activity: UIActivityIndicatorView = UIActivityIndicatorView(style: .gray)
        view.addSubview(activity)
        
        activity.translatesAutoresizingMaskIntoConstraints = false
        activity.addConstraint(NSLayoutConstraint(item: activity, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: activity.bounds.size.width))
        activity.addConstraint(NSLayoutConstraint(item: activity, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: activity.bounds.size.height))
        view.addConstraint(NSLayoutConstraint(item: activity, attribute: .centerXWithinMargins, relatedBy: .equal, toItem: view, attribute: .centerXWithinMargins, multiplier: 1.0, constant: 0.0))
        view.addConstraint(NSLayoutConstraint(item: activity, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottomMargin, multiplier: 1.0, constant: -20.0))
        
        activity.startAnimating()
    }
    
    func addImageView(withImage image: UIImage, animated: Bool) {
        var stickerImageViewLength: CGFloat = 100.0
        if #available(iOS 9.0, *) {
            stickerImageViewLength = 125
        }
        
        let stickerImageView: UIImageView = UIImageView(image: image)
        stickerImageView.translatesAutoresizingMaskIntoConstraints = false
        
        if animated {
            if let images = image.images {
                stickerImageView.animationImages = images
                stickerImageView.animationDuration = image.duration
                
                stickerImageView.layer.speed = 1.0
                stickerImageView.layer.timeOffset = 0.0
                stickerImageView.layer.beginTime = 0.0
                
                let animation = QuartzCore.CAKeyframeAnimation(keyPath:"contents")
                let values: [CGImage] = images.compactMap { $0.cgImage }
                animation.values = values
                animation.calculationMode = .discrete
                animation.duration = image.duration
                animation.repeatCount = .greatestFiniteMagnitude
                animation.isRemovedOnCompletion = false
                stickerImageView.layer.add(animation, forKey: "StickerAnimation")
            }
        }
        
        view.addSubview(stickerImageView)
        
        stickerImageView.addConstraint(NSLayoutConstraint(item: stickerImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: stickerImageViewLength))
        stickerImageView.addConstraint(NSLayoutConstraint(item: stickerImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: stickerImageViewLength))
        view.addConstraint(NSLayoutConstraint(item: stickerImageView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 10.0))
        view.addConstraint(NSLayoutConstraint(item: stickerImageView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1.0, constant: 0.0))
    }
}
