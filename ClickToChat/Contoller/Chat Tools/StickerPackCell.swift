//
//  StickerPackCell.swift
//  ClickToChat
//
//  Created by Gabani King on 29/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class StickerPackCell: UITableViewCell {

    @IBOutlet weak var lblNAme: UILabel!
    @IBOutlet weak var lblPublisher: UILabel!
    
    
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var lblTotleCount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
