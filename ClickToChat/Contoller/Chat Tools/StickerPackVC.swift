//
//  StickerPackVC.swift
//  ClickToChat
//
//  Created by Gabani King on 29/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import Toast_Swift
import SDWebImage
import GoogleMobileAds
class StickerPackVC: UIViewController, UITableViewDelegate, UITableViewDataSource, GADFullScreenContentDelegate, GADBannerViewDelegate {
    
    //MARK:- IBOutlet
    @IBOutlet weak var tblView: UITableView!
    var interstitial: GADInterstitialAd!
    var arrStickerPacks: [CCStickerPackedStickerPack] = [CCStickerPackedStickerPack]()
    var arrSticker = NSMutableArray()
        
    //MARK:- View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let request = GADRequest()
        GADInterstitialAd.load(withAdUnitID:interstitial_Ads_ID,request: request,
                               completionHandler: { [self] ad, error in
                                if let error = error {
                                    return
                                }
                                interstitial = ad
                                interstitial?.fullScreenContentDelegate = self
                               }
        )
        
        tblView.delegate = self
        tblView.dataSource = self
        
        self.tblView.contentInset = UIEdgeInsets(top: 5,left: 0,bottom: 5,right: 0)
        
        
        callStickerPackAPI()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Action MEthod
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- UITableView MEthod
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrStickerPacks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StickerPackCell") as! StickerPackCell
        
        let dicData = arrStickerPacks[indexPath.row]
        
        cell.lblNAme.text = dicData.name ?? ""
        cell.lblPublisher.text = dicData.publisher ?? ""
        
        if dicData.stickers.count == 1
        {
            var image1 = dicData.stickers[0].imageFile ?? ""
            image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url1 = URL(string: image1)
            cell.img1.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img1.sd_setImage(with: url1, placeholderImage: UIImage(named: ""))
            
            cell.lblTotleCount.text = ""
            
            
        }
        else if dicData.stickers.count == 2
        {
            var image1 = dicData.stickers[0].imageFile ?? ""
            image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url1 = URL(string: image1)
            cell.img1.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img1.sd_setImage(with: url1, placeholderImage: UIImage(named: ""))
            
            var image2 = dicData.stickers[1].imageFile ?? ""
            image2 = image2.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url2 = URL(string: image2)
            cell.img2.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img2.sd_setImage(with: url2, placeholderImage: UIImage(named: ""))
            
            cell.lblTotleCount.text = ""
            
            
        }
        else if dicData.stickers.count == 3
        {
            var image1 = dicData.stickers[0].imageFile ?? ""
            image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url1 = URL(string: image1)
            cell.img1.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img1.sd_setImage(with: url1, placeholderImage: UIImage(named: ""))
            
            var image2 = dicData.stickers[1].imageFile ?? ""
            image2 = image2.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url2 = URL(string: image2)
            cell.img2.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img2.sd_setImage(with: url2, placeholderImage: UIImage(named: ""))
            
            var image3 = dicData.stickers[2].imageFile ?? ""
            image3 = image3.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url3 = URL(string: image3)
            cell.img3.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img3.sd_setImage(with: url3, placeholderImage: UIImage(named: ""))
            
            cell.lblTotleCount.text = ""
            
            
        }
        else if dicData.stickers.count > 4
        {
            var image1 = dicData.stickers[0].imageFile ?? ""
            image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url1 = URL(string: image1)
            cell.img1.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img1.sd_setImage(with: url1, placeholderImage: UIImage(named: ""))
            
            var image2 = dicData.stickers[1].imageFile ?? ""
            image2 = image2.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url2 = URL(string: image2)
            cell.img2.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img2.sd_setImage(with: url2, placeholderImage: UIImage(named: ""))
            
            var image3 = dicData.stickers[2].imageFile ?? ""
            image3 = image3.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url3 = URL(string: image3)
            cell.img3.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img3.sd_setImage(with: url3, placeholderImage: UIImage(named: ""))
            
            var image4 = dicData.stickers[3].imageFile ?? ""
            image4 = image4.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url4 = URL(string: image4)
            cell.img4.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img4.sd_setImage(with: url4, placeholderImage: UIImage(named: ""))
            
            if dicData.stickers.count == 4
            {
                cell.lblTotleCount.text = ""
            }
            else
            {
                cell.lblTotleCount.text = "+ \(dicData.stickers.count - 4)"
            }
            
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            return 135
        }
        else
        {
            return 170
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if interstitial != nil {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
        
        let dicData = arrStickerPacks[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(identifier: "StickerListVC") as! StickerListVC
        vc.dicStickerObject = self.arrSticker.object(at: indexPath.row) as! NSDictionary
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    // MARK: - API Call
    func callStickerPackAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(STICKER_PACKS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        let arrData = responseUser.value(forKey: "sticker_packs") as? NSArray
                        
                        for object in arrData!
                        {
                            let dic = CCStickerPackedStickerPack(fromDictionary: (object as? NSDictionary)!)
                            self.arrStickerPacks.append(dic)
                            self.arrSticker.add(object)
                        }
                        
                        self.tblView.reloadData()
                        
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error)
    {
        
    }
    
    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd)
    {
        
    }
    
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd)
    {
        
    }
    
}
