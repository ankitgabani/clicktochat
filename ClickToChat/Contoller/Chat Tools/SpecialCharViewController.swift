//
//  SpecialCharViewController.swift
//  ClickToChat
//
//  Created by Gabani King on 30/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SpecialCharViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, GADFullScreenContentDelegate, GADBannerViewDelegate {
    
    @IBOutlet weak var collectionViewSC: UICollectionView!
    var interstitial: GADInterstitialAd!
    var arrSpecialChar: [CCSCharacter] = [CCSCharacter]()
    
    let sectionInsets = UIEdgeInsets(top: 12.0,
                                     left: 12.0,
                                     bottom: 12.0,
                                     right: 12.0)
    let itemsPerRow: CGFloat = 6
    
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: widthPerItem)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 12.0, left: 12.0, bottom: 12.0, right: 12.0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        _flowLayout.minimumInteritemSpacing = 12.0
        _flowLayout.minimumLineSpacing = 12.0
        return _flowLayout
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionViewSC.delegate = self
        collectionViewSC.dataSource = self
        
        collectionViewSC.collectionViewLayout = flowLayout
        
        callGetSymbolsAPI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let request = GADRequest()
        GADInterstitialAd.load(withAdUnitID:interstitial_Ads_ID,request: request,
                               completionHandler: { [self] ad, error in
                                if let error = error {
                                    return
                                }
                                interstitial = ad
                                interstitial?.fullScreenContentDelegate = self
                               }
        )
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSpecialChar.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionViewSC.dequeueReusableCell(withReuseIdentifier: "SpecialCharCell", for: indexPath) as! SpecialCharCell
        
        let dicData = arrSpecialChar[indexPath.row]
        
        cell.lblName.text = dicData.character ?? ""
        
        cell.lblName.tag = indexPath.row
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(sender:)))
        cell.lblName.isUserInteractionEnabled = true
        cell.lblName.addGestureRecognizer(longPressRecognizer)
        
        
        return cell
    }
    
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began {
            
            let touchPoint = sender.location(in: self.collectionViewSC)
            if let indexPath = collectionViewSC.indexPathForItem(at: touchPoint) {
                
                print("Long pressed row: \(indexPath.row)")
                
                let dicData = arrSpecialChar[indexPath.row]                
                
                let alert = UIAlertController(title: "\(dicData.character ?? "")", message: "\ncopied.", preferredStyle: .alert)
                
                let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                    
                    UIPasteboard.general.string = dicData.character ?? ""
                    
                    if self.interstitial != nil {
                        self.interstitial.present(fromRootViewController: self)
                    } else {
                        print("Ad wasn't ready")
                        
                    }
                })
                alert.addAction(ok)
                
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true)
                })
                
            }
        }
        
    }
    
    
    // MARK: - API Call
    func callGetSymbolsAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGetArray(GET_SYMBOLS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        let arrData = responseUser as? NSArray
                        
                        for obj in arrData! {
                            let dicData = CCSCharacter(fromDictionary: (obj as? NSDictionary)!)
                            self.arrSpecialChar.append(dicData)
                        }
                        
                        self.collectionViewSC.reloadData()
                        
                        
                    } else {
                        
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                
            }
        })
    }
    
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error)
    {
        
    }
    
    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd)
    {
        
    }
    
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd)
    {
        
    }
}
