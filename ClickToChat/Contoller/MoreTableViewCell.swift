//
//  MoreTableViewCell.swift
//  ClickToChat
//
//  Created by Gabani King on 02/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class MoreTableViewCell: UITableViewCell {
    @IBOutlet weak var lblANme: UILabel!
    @IBOutlet weak var imgMore: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
