//
//  TabViewController.swift
//  ClickToChat
//
//  Created by Gabani King on 28/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class TabViewController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.selectedIndex = 3

        // Do any additional setup after loading the view.
    }
  
}
