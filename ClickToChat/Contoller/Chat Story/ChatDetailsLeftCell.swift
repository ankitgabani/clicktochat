//
//  ChatDetailsLeftCell.swift
//  ClickToChat
//
//  Created by Gabani King on 05/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class ChatDetailsLeftCell: UITableViewCell {
   
    @IBOutlet weak var lblNAme: UILabel!
      @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var mainView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
