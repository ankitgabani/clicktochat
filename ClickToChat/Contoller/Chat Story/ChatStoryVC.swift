//
//  ChatStoryVC.swift
//  ClickToChat
//
//  Created by Gabani King on 28/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleMobileAds

class ChatStoryVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource, GADFullScreenContentDelegate, GADBannerViewDelegate {
    
    //MARK:- IBOutlet
    @IBOutlet weak var collectionViewSlider: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var tblView: UITableView!
    
    var interstitial: GADInterstitialAd!
    
    var arrStories: [CCStoriesDATA] = [CCStoriesDATA]()
    
    var timerRR: Timer?
    
    let sectionInsets = UIEdgeInsets(top: 0.0,
                                     left: 0.0,
                                     bottom: 0.0,
                                     right: 0.0)
    let itemsPerRow: CGFloat = 1
    
    var flowLayout: UICollectionViewFlowLayout {
        
        let _flowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            _flowLayout.itemSize = CGSize(width: self.collectionViewSlider.frame.size.width, height: self.collectionViewSlider.frame.size.height)
        }
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0.0
        
        return _flowLayout
        
    }
    
    //MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callGetStoriesAPI()
        timerRR?.invalidate()
        timerRR = nil
        collectionViewSlider.delegate = self
        collectionViewSlider.dataSource = self
        tblView.delegate = self
        tblView.dataSource = self
        self.collectionViewSlider.collectionViewLayout = flowLayout
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let request = GADRequest()
        GADInterstitialAd.load(withAdUnitID:interstitial_Ads_ID,request: request,
                               completionHandler: { [self] ad, error in
                                if let error = error {
                                    return
                                }
                                interstitial = ad
                                interstitial?.fullScreenContentDelegate = self
                               }
        )
        
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    //MARK:- collectionView Method
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrStories.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionViewSlider.dequeueReusableCell(withReuseIdentifier: "ChatStoryCollectionCell", for: indexPath) as!  ChatStoryCollectionCell
        
        let dicData = arrStories[indexPath.row]
        
        cell.lblTitle.text = dicData.title ?? ""
        
        cell.lblAuthorName.text = dicData.author ?? ""
        
        var image1 = dicData.picturelandscape ?? ""
        image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url1 = URL(string: image1)
        cell.imgBg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgBg.sd_setImage(with: url1, placeholderImage: UIImage(named: ""))
        
        cell.btnRead.backgroundColor = App_Color_Blue
        
        cell.btnRead.tag = indexPath.row
        cell.btnRead.addTarget(self, action: #selector(clickedRead(sender:)), for: .touchUpInside)
        
        DispatchQueue.main.async {
            cell.btnRead.layer.cornerRadius = cell.btnRead.frame.height / 2
            cell.btnRead.clipsToBounds = true
        }
        
        
        return cell
    }
    
    //MARK:- tableView Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrStories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "ChatStoryTableViewCell") as! ChatStoryTableViewCell
        
        let dicData = arrStories[indexPath.row]
        
        cell.lblTitle.text = dicData.title ?? ""
        
        cell.lblAuthr.text = dicData.author ?? ""
        
        cell.lblSubtitl.text = dicData.subtitle ?? ""
        
        cell.lblTag.text = dicData.tag ?? ""
        
        
        var image1 = dicData.picture ?? ""
        image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url1 = URL(string: image1)
        cell.imgPic.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgPic.sd_setImage(with: url1, placeholderImage: UIImage(named: ""))
        
        cell.btnRead.tag = indexPath.row
        cell.btnRead.addTarget(self, action: #selector(clickedRead(sender:)), for: .touchUpInside)
        
        
        DispatchQueue.main.async {
            cell.btnRead.layer.cornerRadius = cell.btnRead.frame.height / 2
            cell.btnRead.clipsToBounds = true
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return 186
        }
        else
        {
            return 350
        }
        
    }
    
    @objc func clickedRead(sender: UIButton) {
        
        if interstitial != nil {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatDetailsViewController") as! ChatDetailsViewController
        vc.dicStoriesDATA = arrStories[sender.tag]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let page = scrollView.contentOffset.x/scrollView.frame.size.width
        self.pageController.currentPage = (Int)(page)
    }
    
    /**
     Scroll to Next Cell
     */
    @objc func scrollToNextCell(){
        
        if pageController.currentPage == pageController.numberOfPages - 1 {
            pageController.currentPage = 0
        } else {
            pageController.currentPage += 1
        }
        
        collectionViewSlider.scrollToItem(at: IndexPath(row: pageController.currentPage, section: 0), at: .right, animated: true)
    }
    
    /**
     Invokes Timer to start Automatic Animation with repeat enabled
     */
    func startTimer() {
        
        timerRR = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.scrollToNextCell), userInfo: nil, repeats: true)
    }
    
    
    // MARK: - API Call
    func callGetStoriesAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(GET_STORIES, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["status"] as? String ?? ""
                let data_Message = response?["data"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        self.arrStories.removeAll()
                        
                        let arrData = responseUser.value(forKey: "DATA") as? NSArray
                        
                        for obj in arrData! {
                            
                            let dicData = CCStoriesDATA(fromDictionary: (obj as? NSDictionary)!)
                            self.arrStories.append(dicData)
                        }
                        
                        self.collectionViewSlider.reloadData()
                        self.tblView.reloadData()
                        
                        self.pageController.numberOfPages = self.arrStories.count
                        self.startTimer()
                        
                        
                    } else {
                        self.view.makeToast(data_Message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(data_Message)
                    
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["data"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
