//
//  ChatStoryCollectionCell.swift
//  ClickToChat
//
//  Created by Gabani King on 01/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class ChatStoryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var lblAuthorName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnRead: UIButton!
}
