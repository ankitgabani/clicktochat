//
//  ChatStoryTableViewCell.swift
//  ClickToChat
//
//  Created by Gabani King on 01/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class ChatStoryTableViewCell: UITableViewCell {

    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAuthr: UILabel!
    @IBOutlet weak var lblSubtitl: UILabel!
    @IBOutlet weak var lblTag: UILabel!
    
    @IBOutlet weak var btnRead: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnRead.backgroundColor = App_Color_Blue
        lblTag.textColor = App_Color_Blue
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
