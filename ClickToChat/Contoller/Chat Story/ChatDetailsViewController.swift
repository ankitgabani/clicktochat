//
//  ChatDetailsViewController.swift
//  ClickToChat
//
//  Created by Gabani King on 05/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ChatDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, GADFullScreenContentDelegate, GADBannerViewDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bottombannerView: GADBannerView!
    @IBOutlet weak var footerViewTap: UIView!
    
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var imgGIF: UIImageView!
    
    var dicStoriesDATA: CCStoriesDATA?
    
    var arrStoriesConversation: [CCStoriesConversation] = [CCStoriesConversation]()
    
    var bannerView = GADBannerView()
    
    var isFirstTime = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        let data = UserDefaults.standard.value(forKey: "\(dicStoriesDATA!.storyId!)") as? Data
        
        
        if data == nil{
            
            let array = dicStoriesDATA?.conversations
            let firstObject = array?.first
            
            let data = NSKeyedArchiver.archivedData(withRootObject: firstObject)
            
            UserDefaults.standard.setValue(data, forKey: "\(dicStoriesDATA!.storyId!)")
            UserDefaults.standard.synchronize()
            
            self.arrStoriesConversation.append(firstObject!)
            
            self.tblView.reloadData()
        }
        else{
            
            let lastObject = NSKeyedUnarchiver.unarchiveObject(with: data!) as! CCStoriesConversation
            
            let array = dicStoriesDATA?.conversations
            
            let index = array!.index(where: { (item) -> Bool in
                item.chatId == lastObject.chatId
            })
            
            if index! > 0{
                var arrayObject = array![0..<index!+1]
                
                for j in 0..<arrayObject.count{
                    let objc = arrayObject[j]
                    
                    self.arrStoriesConversation.append(objc)
                }
            }
            else{
                self.arrStoriesConversation.append(lastObject)
            }
            
            
            
            self.tblView.reloadData()
        }
        
        
        isFirstTime = true
        
        if (self.arrStoriesConversation.count) > 0
        {
            tblView.reloadData()
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: (self.arrStoriesConversation.count) - 1, section: 0)
                self.tblView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
        
        self.tabBarController?.tabBar.isHidden = true
        
        viewBG.isHidden = false
        
        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "TapToStart", withExtension: "gif")!)
        let advTimeGif = UIImage.gifImageWithData(data: imageData! as NSData)
        let imageView2 = UIImageView(image: advTimeGif)
        imageView2.contentMode = .scaleAspectFill
        DispatchQueue.main.async {
            imageView2.frame = CGRect(x: 0, y: 0, width:
                                        self.imgGIF.frame.size.width, height: self.imgGIF.frame.size.height)
            
        }
        imgGIF.addSubview(imageView2)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        viewBG.addGestureRecognizer(tap)
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.handleTapAddData(_:)))
        tblView.addGestureRecognizer(tap1)
        
        bottombannerView.delegate = self
        bottombannerView.rootViewController = self
        bottombannerView.adUnitID = banner_Ads_ID
        bottombannerView.load(GADRequest())
        
        self.lblTitle.text = dicStoriesDATA?.title
        
        tblView.delegate = self
        tblView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        viewBG.isHidden = true
    }
    
    @objc func handleTapAddData(_ sender: UITapGestureRecognizer? = nil) {
        
        
        let nextLoadIndex = self.arrStoriesConversation.count
        
        let array = dicStoriesDATA?.conversations
        
        if nextLoadIndex < array!.count{
            
            let nextObject = array![nextLoadIndex]
            
            let data = NSKeyedArchiver.archivedData(withRootObject: nextObject)
            
            UserDefaults.standard.setValue(data, forKey: "\(dicStoriesDATA!.storyId!)")
            UserDefaults.standard.synchronize()
            
            self.arrStoriesConversation.append(nextObject)
            self.tblView.reloadData()
            
            isFirstTime = false
            
            if (arrStoriesConversation.count) > 0
            {
                tblView.reloadData()
                DispatchQueue.main.async {
                    let indexPath = IndexPath(row: (self.arrStoriesConversation.count) - 1, section: 0)
                    self.tblView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            }
            
        }
        else
        {
            AppUtilites.showAlert(title: "", message: "Story end !", cancelButtonTitle: "OK")
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrStoriesConversation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dicData = arrStoriesConversation[indexPath.row]
        
        if dicData.issent == "0"
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "ChatDetailsLeftCell") as! ChatDetailsLeftCell
            
            cell.lblNAme.text = dicData.name ?? ""
            cell.lblMessage.text = dicData.message ?? ""
            
            return cell
        }
        else
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "ChatDetailsRightCell") as! ChatDetailsRightCell
            
            cell.lblNAme.text = dicData.name ?? ""
            cell.lblMessage.text = dicData.message ?? ""
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = Bundle.main.loadNibNamed("FooterSectionView", owner: self, options: [:])?.first as! FooterSectionView
        footerView.backgroundColor = UIColor.clear
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.handleTapAddData(_:)))
        footerView.addGestureRecognizer(tap2)
        
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if isFirstTime == true
        {
            return 1
        }
        else
        {
            return CGFloat(tblView.frame.height / 2)
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("bannerViewDidReceiveAd")
    }
    
    func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
        print("bannerView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    func bannerViewDidRecordImpression(_ bannerView: GADBannerView) {
        print("bannerViewDidRecordImpression")
    }
    
    func bannerViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("bannerViewWillPresentScreen")
    }
    
    func bannerViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("bannerViewWillDIsmissScreen")
    }
    
    func bannerViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("bannerViewDidDismissScreen")
    }
}
