//
//  InAppPurchaseVC.swift
//  ClickToChat
//
//  Created by Gabani King on 10/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import DLRadioButton
import StoreKit

class InAppPurchaseVC: UIViewController, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    @IBOutlet weak var btnYear: DLRadioButton!
    @IBOutlet weak var btnMonth: DLRadioButton!
    
    @IBOutlet weak var lblFree: UILabel!
    @IBOutlet weak var btnContiune: UIButton!
        
    var transactionInProgress = false
    var selectedProductIndex: Int!
    
    var productIDs: Array<String?> = []
    
    var productsArray: Array<SKProduct?> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblFree.backgroundColor = App_Color_Blue
        btnContiune.backgroundColor = App_Color_Blue
        
        btnYear.isSelected = false
        btnMonth.isSelected = false
        btnYear.iconColor = UIColor(red: 134/255, green: 130/255, blue: 133/255, alpha: 1)
        btnMonth.iconColor = UIColor(red: 134/255, green: 130/255, blue: 133/255, alpha: 1)
        
        
        selectedProductIndex = 0
        
//        productIDs.append(Constants.AppBundleIdentifier)
        
        requestProductInfo()
        
        SKPaymentQueue.default().add(self as SKPaymentTransactionObserver)
        // Do any additional setup after loading the view.
    }
    
    func requestProductInfo() {
        if SKPaymentQueue.canMakePayments() {
            let productIdentifiers = NSSet(array: productIDs as [Any])
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers as Set<NSObject> as! Set<String>)
            
            productRequest.delegate = self
            productRequest.start()
        }
        else {
            print("Cannot perform In App Purchases.")
        }
    }
    
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        if response.products.count != 0 {
            for product in response.products {
                productsArray.append(product )
            }
            
            
        }
        else {
            print("There are no products.")
        }
    }
    
    func showActions() {
        if transactionInProgress {
            return
        }
        
        let actionSheetController = UIAlertController(title: "ClickToChat", message: "What do you want to do?", preferredStyle: UIAlertController.Style.actionSheet)
        
        let buyAction = UIAlertAction(title: "Buy", style: UIAlertAction.Style.default) { (action) -> Void in
            let payment = SKPayment(product: (self.productsArray[self.selectedProductIndex])!)
            SKPaymentQueue.default().add(payment)
            self.transactionInProgress = true
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) -> Void in
            
        }
        
        actionSheetController.addAction(buyAction)
        actionSheetController.addAction(cancelAction)
        
        present(actionSheetController, animated: true, completion: nil)
    }
    
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased, .restored:
                print("Transaction completed successfully")
                SKPaymentQueue.default().finishTransaction(transaction)
                transactionInProgress = false
                
            case .failed:
                print("Transaction failed");
                SKPaymentQueue.default().finishTransaction(transaction)
                transactionInProgress = false
                
            default:
                print(transaction.transactionState.rawValue)
            }
        }
    }
    
    
    @IBAction func clickedBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedYear(_ sender: Any) {
        btnYear.isSelected = true
        btnMonth.isSelected = false
        
        btnYear.iconColor = UIColor.white
        btnYear.indicatorColor = UIColor(red: 255/255, green: 193/255, blue: 0/255, alpha: 1)
        btnMonth.iconColor = UIColor(red: 134/255, green: 130/255, blue: 133/255, alpha: 1)
    }
    
    @IBAction func clickedMonth(_ sender: Any) {
        btnMonth.isSelected = true
        btnYear.isSelected = false
        
        btnMonth.iconColor = UIColor.white
        btnMonth.indicatorColor = UIColor(red: 255/255, green: 193/255, blue: 0/255, alpha: 1)
        btnYear.iconColor = UIColor(red: 134/255, green: 130/255, blue: 133/255, alpha: 1)
    }
    
    @IBAction func clickedContinue(_ sender: Any) {
        showActions()
    }
    
    @IBAction func clickedRestore(_ sender: Any) {
        if (SKPaymentQueue.canMakePayments()) {
          SKPaymentQueue.default().restoreCompletedTransactions()
        }
    }
    
    @IBAction func clickedTU(_ sender: Any) {
        if let url = URL(string: "https://docs.google.com/document/d/1D7KLRNsUTUdvNGnPm4Lt7LOFYlMinVzO3aafqizK_t8/mobilebasic") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func clickedPP(_ sender: Any) {
        if let url = URL(string: "https://docs.google.com/document/d/1k2Rtqw0BbPk0CawwkAvd_HjeNTx6vnWf19w_c9CltGQ/mobilebasic") {
            UIApplication.shared.open(url)
        }
    }
    
    
}

