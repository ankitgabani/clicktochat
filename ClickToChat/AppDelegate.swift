//
//  AppDelegate.swift
//  ClickToChat
//
//  Created by Gabani King on 28/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import IQKeyboardManager
import GoogleMobileAds
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var dicDetails = NSDictionary()
    var isShowKeypadData = false
    
    var arrCurrentMessageList = NSMutableArray()
    var arrKeypadNumber: [CCKeypadNumber] = [CCKeypadNumber]()
    var dicKeypadNumber = CCKeypadNumber()
    var isUpdateKeypad = false
    var isFirstTimeChat = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
   
        arrCurrentMessageList = getCurrentMessageList()
        arrKeypadNumber = getKeypadNumberList()
        
        
        print("Send *********** \(arrKeypadNumber.count) Appdelegate *********")

        print("Send Conunt *********** \(getKeypadNumberList().count) Appdelegate *********")

        
        IQKeyboardManager.shared().isEnabled = true
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home: TabViewController = mainStoryboard.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
        home.selectedIndex = 3
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        self.window?.rootViewController = homeNavigation
        self.window?.makeKeyAndVisible()
        return true
    }
    
    func saveCurrentMessageList(array: NSMutableArray)
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: array)
        UserDefaults.standard.setValue(data, forKey: "CurrentMessageList")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentMessageList() -> NSMutableArray
    {
        if let data = UserDefaults.standard.object(forKey: "CurrentMessageList"){
            
            let arrayObjc = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
            return arrayObjc as! NSMutableArray
        }
        return NSMutableArray()
    }
    
    func saveKeypadNumberList(array: [CCKeypadNumber])
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: array)
        UserDefaults.standard.setValue(data, forKey: "KeypadNumberList")
        UserDefaults.standard.synchronize()
    }
    
    func getKeypadNumberList() -> [CCKeypadNumber]
    {
        if let data = UserDefaults.standard.object(forKey: "KeypadNumberList")
        {
            let arrayObjc = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
            return arrayObjc as! [CCKeypadNumber]
        }
        return [CCKeypadNumber]()
    }
    
    
}

