//
//  Constants.swift
//  Swoosh Rider
//
//  Created by Gabani King on 10/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import Foundation
import UIKit

let BASE_URL = "https://www.appcodism.com/adamprojects/"

let STICKER_PACKS = "getstickerpacks.php?ts=23344334"

let GET_JOKES = "getjokes.php"

let GET_QUOTES = "getquotes.php"

let GET_SYMBOLS = "getsymbols.php"

let GET_STORIES = "getstories.php"

let appDelegate = UIApplication.shared.delegate as! AppDelegate

let interstitial_Ads_ID = "ca-app-pub-3940256099942544/4411468910"

let banner_Ads_ID = "ca-app-pub-3940256099942544/2934735716"


//
//"https://www.appcodism.com/adamprojects/getstickerpacks.php?ts=23344334"
