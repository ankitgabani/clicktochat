//
//	CCSCharacter.swift
//
//	Create by mac on 30/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CCSCharacter : NSObject, NSCoding{

	var character : String!
	var font : String!
	var html : String!
	var keywords : String!
	var name : String!
	var unicode : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		character = dictionary["character"] as? String == nil ? "" : dictionary["character"] as? String
		font = dictionary["font"] as? String == nil ? "" : dictionary["font"] as? String
		html = dictionary["html"] as? String == nil ? "" : dictionary["html"] as? String
		keywords = dictionary["keywords"] as? String == nil ? "" : dictionary["keywords"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		unicode = dictionary["unicode"] as? String == nil ? "" : dictionary["unicode"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if character != nil{
			dictionary["character"] = character
		}
		if font != nil{
			dictionary["font"] = font
		}
		if html != nil{
			dictionary["html"] = html
		}
		if keywords != nil{
			dictionary["keywords"] = keywords
		}
		if name != nil{
			dictionary["name"] = name
		}
		if unicode != nil{
			dictionary["unicode"] = unicode
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         character = aDecoder.decodeObject(forKey: "character") as? String
         font = aDecoder.decodeObject(forKey: "font") as? String
         html = aDecoder.decodeObject(forKey: "html") as? String
         keywords = aDecoder.decodeObject(forKey: "keywords") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         unicode = aDecoder.decodeObject(forKey: "unicode") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if character != nil{
			aCoder.encode(character, forKey: "character")
		}
		if font != nil{
			aCoder.encode(font, forKey: "font")
		}
		if html != nil{
			aCoder.encode(html, forKey: "html")
		}
		if keywords != nil{
			aCoder.encode(keywords, forKey: "keywords")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if unicode != nil{
			aCoder.encode(unicode, forKey: "unicode")
		}

	}

}