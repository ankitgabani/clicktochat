//
//	CCStoriesDATA.swift
//
//	Create by mac on 2/7/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CCStoriesDATA : NSObject, NSCoding{

	var author : String!
	var conversations : [CCStoriesConversation]!
	var picture : String!
	var picturelandscape : String!
	var storyId : String!
	var subtitle : String!
	var tag : String!
	var title : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		author = dictionary["author"] as? String == nil ? "" : dictionary["author"] as? String
		conversations = [CCStoriesConversation]()
		if let conversationsArray = dictionary["conversations"] as? [NSDictionary]{
			for dic in conversationsArray{
				let value = CCStoriesConversation(fromDictionary: dic)
				conversations.append(value)
			}
		}
		picture = dictionary["picture"] as? String == nil ? "" : dictionary["picture"] as? String
		picturelandscape = dictionary["picturelandscape"] as? String == nil ? "" : dictionary["picturelandscape"] as? String
		storyId = dictionary["story_id"] as? String == nil ? "" : dictionary["story_id"] as? String
		subtitle = dictionary["subtitle"] as? String == nil ? "" : dictionary["subtitle"] as? String
		tag = dictionary["tag"] as? String == nil ? "" : dictionary["tag"] as? String
		title = dictionary["title"] as? String == nil ? "" : dictionary["title"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if author != nil{
			dictionary["author"] = author
		}
		if conversations != nil{
			var dictionaryElements = [NSDictionary]()
			for conversationsElement in conversations {
				dictionaryElements.append(conversationsElement.toDictionary())
			}
			dictionary["conversations"] = dictionaryElements
		}
		if picture != nil{
			dictionary["picture"] = picture
		}
		if picturelandscape != nil{
			dictionary["picturelandscape"] = picturelandscape
		}
		if storyId != nil{
			dictionary["story_id"] = storyId
		}
		if subtitle != nil{
			dictionary["subtitle"] = subtitle
		}
		if tag != nil{
			dictionary["tag"] = tag
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         author = aDecoder.decodeObject(forKey: "author") as? String
         conversations = aDecoder.decodeObject(forKey: "conversations") as? [CCStoriesConversation]
         picture = aDecoder.decodeObject(forKey: "picture") as? String
         picturelandscape = aDecoder.decodeObject(forKey: "picturelandscape") as? String
         storyId = aDecoder.decodeObject(forKey: "story_id") as? String
         subtitle = aDecoder.decodeObject(forKey: "subtitle") as? String
         tag = aDecoder.decodeObject(forKey: "tag") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if author != nil{
			aCoder.encode(author, forKey: "author")
		}
		if conversations != nil{
			aCoder.encode(conversations, forKey: "conversations")
		}
		if picture != nil{
			aCoder.encode(picture, forKey: "picture")
		}
		if picturelandscape != nil{
			aCoder.encode(picturelandscape, forKey: "picturelandscape")
		}
		if storyId != nil{
			aCoder.encode(storyId, forKey: "story_id")
		}
		if subtitle != nil{
			aCoder.encode(subtitle, forKey: "subtitle")
		}
		if tag != nil{
			aCoder.encode(tag, forKey: "tag")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}

	}

}
