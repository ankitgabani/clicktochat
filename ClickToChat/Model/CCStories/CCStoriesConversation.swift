//
//	CCStoriesConversation.swift
//
//	Create by mac on 2/7/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CCStoriesConversation : NSObject, NSCoding{

	var chatId : String!
	var issent : String!
	var message : String!
	var name : String!
	var picture : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		chatId = dictionary["chat_id"] as? String == nil ? "" : dictionary["chat_id"] as? String
		issent = dictionary["issent"] as? String == nil ? "" : dictionary["issent"] as? String
		message = dictionary["message"] as? String == nil ? "" : dictionary["message"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		picture = dictionary["picture"] as? String == nil ? "" : dictionary["picture"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if chatId != nil{
			dictionary["chat_id"] = chatId
		}
		if issent != nil{
			dictionary["issent"] = issent
		}
		if message != nil{
			dictionary["message"] = message
		}
		if name != nil{
			dictionary["name"] = name
		}
		if picture != nil{
			dictionary["picture"] = picture
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         chatId = aDecoder.decodeObject(forKey: "chat_id") as? String
         issent = aDecoder.decodeObject(forKey: "issent") as? String
         message = aDecoder.decodeObject(forKey: "message") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         picture = aDecoder.decodeObject(forKey: "picture") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if chatId != nil{
			aCoder.encode(chatId, forKey: "chat_id")
		}
		if issent != nil{
			aCoder.encode(issent, forKey: "issent")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if picture != nil{
			aCoder.encode(picture, forKey: "picture")
		}

	}

}