//
//	CCStickerPacked.swift
//
//	Create by mac on 29/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CCStickerPacked : NSObject, NSCoding{

	var androidPlayStoreLink : String!
	var iosAppStoreLink : String!
	var stickerPacks : [CCStickerPackedStickerPack]!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		androidPlayStoreLink = dictionary["android_play_store_link"] as? String == nil ? "" : dictionary["android_play_store_link"] as? String
		iosAppStoreLink = dictionary["ios_app_store_link"] as? String == nil ? "" : dictionary["ios_app_store_link"] as? String
		stickerPacks = [CCStickerPackedStickerPack]()
		if let stickerPacksArray = dictionary["sticker_packs"] as? [NSDictionary]{
			for dic in stickerPacksArray{
				let value = CCStickerPackedStickerPack(fromDictionary: dic)
				stickerPacks.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if androidPlayStoreLink != nil{
			dictionary["android_play_store_link"] = androidPlayStoreLink
		}
		if iosAppStoreLink != nil{
			dictionary["ios_app_store_link"] = iosAppStoreLink
		}
		if stickerPacks != nil{
			var dictionaryElements = [NSDictionary]()
			for stickerPacksElement in stickerPacks {
				dictionaryElements.append(stickerPacksElement.toDictionary())
			}
			dictionary["sticker_packs"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         androidPlayStoreLink = aDecoder.decodeObject(forKey: "android_play_store_link") as? String
         iosAppStoreLink = aDecoder.decodeObject(forKey: "ios_app_store_link") as? String
         stickerPacks = aDecoder.decodeObject(forKey: "sticker_packs") as? [CCStickerPackedStickerPack]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if androidPlayStoreLink != nil{
			aCoder.encode(androidPlayStoreLink, forKey: "android_play_store_link")
		}
		if iosAppStoreLink != nil{
			aCoder.encode(iosAppStoreLink, forKey: "ios_app_store_link")
		}
		if stickerPacks != nil{
			aCoder.encode(stickerPacks, forKey: "sticker_packs")
		}

	}

}