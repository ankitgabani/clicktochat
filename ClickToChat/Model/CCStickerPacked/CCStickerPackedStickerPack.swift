//
//	CCStickerPackedStickerPack.swift
//
//	Create by mac on 29/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CCStickerPackedStickerPack : NSObject, NSCoding{

	var identifier : String!
	var licenseAgreementWebsite : String!
	var name : String!
	var packImage1 : String!
	var privacyPolicyWebsite : String!
	var publisher : String!
	var publisherEmail : String!
	var publisherWebsite : String!
	var searchKeywords : String!
	var stickers : [CCStickerPackedSticker]!
	var trayImageFile : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		identifier = dictionary["identifier"] as? String == nil ? "" : dictionary["identifier"] as? String
		licenseAgreementWebsite = dictionary["license_agreement_website"] as? String == nil ? "" : dictionary["license_agreement_website"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		packImage1 = dictionary["pack_image1"] as? String == nil ? "" : dictionary["pack_image1"] as? String
		privacyPolicyWebsite = dictionary["privacy_policy_website"] as? String == nil ? "" : dictionary["privacy_policy_website"] as? String
		publisher = dictionary["publisher"] as? String == nil ? "" : dictionary["publisher"] as? String
		publisherEmail = dictionary["publisher_email"] as? String == nil ? "" : dictionary["publisher_email"] as? String
		publisherWebsite = dictionary["publisher_website"] as? String == nil ? "" : dictionary["publisher_website"] as? String
		searchKeywords = dictionary["search_keywords"] as? String == nil ? "" : dictionary["search_keywords"] as? String
		stickers = [CCStickerPackedSticker]()
		if let stickersArray = dictionary["stickers"] as? [NSDictionary]{
			for dic in stickersArray{
				let value = CCStickerPackedSticker(fromDictionary: dic)
				stickers.append(value)
			}
		}
		trayImageFile = dictionary["tray_image_file"] as? String == nil ? "" : dictionary["tray_image_file"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if identifier != nil{
			dictionary["identifier"] = identifier
		}
		if licenseAgreementWebsite != nil{
			dictionary["license_agreement_website"] = licenseAgreementWebsite
		}
		if name != nil{
			dictionary["name"] = name
		}
		if packImage1 != nil{
			dictionary["pack_image1"] = packImage1
		}
		if privacyPolicyWebsite != nil{
			dictionary["privacy_policy_website"] = privacyPolicyWebsite
		}
		if publisher != nil{
			dictionary["publisher"] = publisher
		}
		if publisherEmail != nil{
			dictionary["publisher_email"] = publisherEmail
		}
		if publisherWebsite != nil{
			dictionary["publisher_website"] = publisherWebsite
		}
		if searchKeywords != nil{
			dictionary["search_keywords"] = searchKeywords
		}
		if stickers != nil{
			var dictionaryElements = [NSDictionary]()
			for stickersElement in stickers {
				dictionaryElements.append(stickersElement.toDictionary())
			}
			dictionary["stickers"] = dictionaryElements
		}
		if trayImageFile != nil{
			dictionary["tray_image_file"] = trayImageFile
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         identifier = aDecoder.decodeObject(forKey: "identifier") as? String
         licenseAgreementWebsite = aDecoder.decodeObject(forKey: "license_agreement_website") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         packImage1 = aDecoder.decodeObject(forKey: "pack_image1") as? String
         privacyPolicyWebsite = aDecoder.decodeObject(forKey: "privacy_policy_website") as? String
         publisher = aDecoder.decodeObject(forKey: "publisher") as? String
         publisherEmail = aDecoder.decodeObject(forKey: "publisher_email") as? String
         publisherWebsite = aDecoder.decodeObject(forKey: "publisher_website") as? String
         searchKeywords = aDecoder.decodeObject(forKey: "search_keywords") as? String
         stickers = aDecoder.decodeObject(forKey: "stickers") as? [CCStickerPackedSticker]
         trayImageFile = aDecoder.decodeObject(forKey: "tray_image_file") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if identifier != nil{
			aCoder.encode(identifier, forKey: "identifier")
		}
		if licenseAgreementWebsite != nil{
			aCoder.encode(licenseAgreementWebsite, forKey: "license_agreement_website")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if packImage1 != nil{
			aCoder.encode(packImage1, forKey: "pack_image1")
		}
		if privacyPolicyWebsite != nil{
			aCoder.encode(privacyPolicyWebsite, forKey: "privacy_policy_website")
		}
		if publisher != nil{
			aCoder.encode(publisher, forKey: "publisher")
		}
		if publisherEmail != nil{
			aCoder.encode(publisherEmail, forKey: "publisher_email")
		}
		if publisherWebsite != nil{
			aCoder.encode(publisherWebsite, forKey: "publisher_website")
		}
		if searchKeywords != nil{
			aCoder.encode(searchKeywords, forKey: "search_keywords")
		}
		if stickers != nil{
			aCoder.encode(stickers, forKey: "stickers")
		}
		if trayImageFile != nil{
			aCoder.encode(trayImageFile, forKey: "tray_image_file")
		}

	}

}
