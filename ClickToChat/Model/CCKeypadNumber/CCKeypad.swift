//
//  CCKeypad.swift
//  ClickToChat
//
//  Created by Gabani King on 09/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import Foundation

class CCKeypad : NSObject, NSCoding{
    
    var data : [CCKeypadNumber]!
    
    /**
     * Overiding init method
     */
    init(fromDictionary dictionary: NSDictionary)
    {
        super.init()
        parseJSONData(fromDictionary: dictionary)
    }
    
    /**
     * Overiding init method
     */
    override init(){
    }
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    @objc func parseJSONData(fromDictionary dictionary: NSDictionary)
    {
        data = [CCKeypadNumber]()
        if let dATAArray = dictionary["data"] as? [NSDictionary]{
            for dic in dATAArray{
                let value = CCKeypadNumber(fromDictionary: dic)
                data.append(value)
            }
        }
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if data != nil{
            var dictionaryElements = [NSDictionary]()
            for dATAElement in data {
                dictionaryElements.append(dATAElement.toDictionary())
            }
            dictionary["data"] = dictionaryElements
        }
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        data = aDecoder.decodeObject(forKey: "data") as? [CCKeypadNumber]
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    public func encode(with aCoder: NSCoder)
    {
        if data != nil{
            aCoder.encode(data, forKey: "data")
        }
        
        
    }
    
}
