//
//  CCKeypadNumber.swift
//  ClickToChat
//
//  Created by Gabani King on 09/07/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//
import Foundation

class CCKeypadNumber : NSObject, NSCoding{
    
    var userMobile : String!
    var UserCode : String!
    var UserType : String!
    var UserName : String!
    var UserMessage : String!
    
    
    /**
     * Overiding init method
     */
    init(fromDictionary dictionary: NSDictionary)
    {
        super.init()
        parseJSONData(fromDictionary: dictionary)
    }
    
    /**
     * Overiding init method
     */
    override init(){
    }
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    @objc func parseJSONData(fromDictionary dictionary: NSDictionary)
    {
        userMobile = dictionary["userMobile"] as? String == nil ? "" : dictionary["userMobile"] as? String
        UserCode = dictionary["UserCode"] as? String == nil ? "" : dictionary["UserCode"] as? String
        UserType = dictionary["UserType"] as? String == nil ? "" : dictionary["UserType"] as? String
        UserName = dictionary["UserName"] as? String == nil ? "" : dictionary["UserName"] as? String
        UserMessage = dictionary["UserMessage"] as? String == nil ? "" : dictionary["UserMessage"] as? String
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if userMobile != nil{
            dictionary["userMobile"] = userMobile
        }
        if UserCode != nil{
            dictionary["UserCode"] = UserCode
        }
        if UserType != nil{
            dictionary["UserType"] = UserType
        }
        if UserName != nil{
            dictionary["UserName"] = UserName
        }
        if UserMessage != nil{
            dictionary["UserMessage"] = UserMessage
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        userMobile = aDecoder.decodeObject(forKey: "userMobile") as? String
        UserCode = aDecoder.decodeObject(forKey: "UserCode") as? String
        UserType = aDecoder.decodeObject(forKey: "UserType") as? String
        UserName = aDecoder.decodeObject(forKey: "UserName") as? String
        UserMessage = aDecoder.decodeObject(forKey: "UserMessage") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    public func encode(with aCoder: NSCoder)
    {
        if userMobile != nil{
            aCoder.encode(userMobile, forKey: "userMobile")
        }
        if UserCode != nil{
            aCoder.encode(UserCode, forKey: "UserCode")
        }
        if UserType != nil{
            aCoder.encode(UserType, forKey: "UserType")
        }
        if UserName != nil{
            aCoder.encode(UserName, forKey: "UserName")
        }
        if UserMessage != nil{
            aCoder.encode(UserMessage, forKey: "UserMessage")
        }
        
    }
    
}
