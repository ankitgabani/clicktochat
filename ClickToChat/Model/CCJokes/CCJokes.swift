//
//	CCJokes.swift
//
//	Create by mac on 30/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CCJokes : NSObject, NSCoding{

	var dATA : [CCJokesDATA]!
	var mESSAGE : String!
	var sUCCESS : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		dATA = [CCJokesDATA]()
		if let dATAArray = dictionary["DATA"] as? [NSDictionary]{
			for dic in dATAArray{
				let value = CCJokesDATA(fromDictionary: dic)
				dATA.append(value)
			}
		}
		mESSAGE = dictionary["MESSAGE"] as? String == nil ? "" : dictionary["MESSAGE"] as? String
		sUCCESS = dictionary["SUCCESS"] as? String == nil ? "" : dictionary["SUCCESS"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if dATA != nil{
			var dictionaryElements = [NSDictionary]()
			for dATAElement in dATA {
				dictionaryElements.append(dATAElement.toDictionary())
			}
			dictionary["DATA"] = dictionaryElements
		}
		if mESSAGE != nil{
			dictionary["MESSAGE"] = mESSAGE
		}
		if sUCCESS != nil{
			dictionary["SUCCESS"] = sUCCESS
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         dATA = aDecoder.decodeObject(forKey: "DATA") as? [CCJokesDATA]
         mESSAGE = aDecoder.decodeObject(forKey: "MESSAGE") as? String
         sUCCESS = aDecoder.decodeObject(forKey: "SUCCESS") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if dATA != nil{
			aCoder.encode(dATA, forKey: "DATA")
		}
		if mESSAGE != nil{
			aCoder.encode(mESSAGE, forKey: "MESSAGE")
		}
		if sUCCESS != nil{
			aCoder.encode(sUCCESS, forKey: "SUCCESS")
		}

	}

}