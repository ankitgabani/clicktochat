//
//	CCJokesDATA.swift
//
//	Create by mac on 30/6/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CCJokesDATA : NSObject, NSCoding{

	var author : String!
	var quoteId : String!
	var text : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		author = dictionary["author"] as? String == nil ? "" : dictionary["author"] as? String
		quoteId = dictionary["quote_id"] as? String == nil ? "" : dictionary["quote_id"] as? String
		text = dictionary["text"] as? String == nil ? "" : dictionary["text"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if author != nil{
			dictionary["author"] = author
		}
		if quoteId != nil{
			dictionary["quote_id"] = quoteId
		}
		if text != nil{
			dictionary["text"] = text
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         author = aDecoder.decodeObject(forKey: "author") as? String
         quoteId = aDecoder.decodeObject(forKey: "quote_id") as? String
         text = aDecoder.decodeObject(forKey: "text") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if author != nil{
			aCoder.encode(author, forKey: "author")
		}
		if quoteId != nil{
			aCoder.encode(quoteId, forKey: "quote_id")
		}
		if text != nil{
			aCoder.encode(text, forKey: "text")
		}

	}

}